const { describe } = require("node:test");
const {
  findDuplicates,
  flattenArray,
  isPalindrome,
  duplicateCount,
  reverseWords,
  mergeArrays,
  findMissingNumber,
  rotateArray,
  findMinMax,
  firstNonRepeatingCharacter,
  sotrWord,
  findAllAnagrams,
  fibonacci,
  noRepetition,
  longestSubstring,
  areAnagrams,
  findIntersection,
  findMostFrequent,
  isPrimeNumber,
  reverseWordsInSentence,
  findMaxMinDifference,
  findPairsWithSum,
  isPermutationPalindrome,
  isPangram,
  findLongestNonDecraisingSubsequence,
  findMaxProductOfTwo,
  findLongestIncreasingSubsequence,
  findPairsWithSum2,
  findBigrams,
  checkBrackets,
  analyzeText,
  quickSort,
  findMissingNumber2,
  capitalizeWords,
  validateEmail,
  mergeArrays2,
  binarySearch,
  findMaxConsecutiveOnes,
  validatePassword,
  findDuplicate,
  findPeakElement,
  maximumSubarraySum,
  sortByStringLength,
  countTriplets,
  findCommonElements,
  findUniqueElements,
  mergeSort,
  maxDiff,
  removeDuplicates,
  isIsogram,
  findMaxDifference,
  maxProductOfTwo,
  hasZeroSumTriplets,
  isPerfectNumber,
  findShortestPath,
  findCycleInGraph,
  sumPairExists,
  maxDifference,
  sortByDigitSum,
  numberOfUniqueness,
  maxProfit,
  findPairsWithDifference,
  calculateSumExceptSelf,
  findEqualPairs,
  parsingDom,
  topologicalSort,
  longestSubstringWithoutRepeating,
  decimalToBinary,
  maxSubarraySumLessThanTarget,
  longestCommonSubsequenceLength,
  longestCommonSubsequence,
  findLongestIncreasingSubsequenceLength,
  findLongestIncreasingSubsequence2,
  gcd,
  sumOfDigits,
  perfectNumbersLowerThan,
  longestPalindrome,
  longestVowelSubstring,
  countUniqueElements,
  countingSort,
  maxSubarraySum,
  countWords,
  binaryToDecimal,
  longestZeroSumSubarray,
  longestSubstringWithoutRepeatingCharacters,
  treeConstructor,
  lengthOfLongestSubstring,
  maxSubarray,
  missingNumber,
  longestIncreasingSubsequence,
  isHappyNumber,
  longestPalindromicSubstring,
  firstUniqChar,
  findShortestPathMaze
} = require("../index.js");

describe("findDuplicates", () => {
  it("should return an empty array when no duplicates are found", () => {
    const array = [1, 2, 3, 4, 5];
    expect(findDuplicates(array)).toEqual([]);
  });

  it("should return an array with duplicate elements when duplicates are found", () => {
    const array = [1, 2, 3, 4, 5, 2, 6, 7, 8, 9];
    expect(findDuplicates(array)).toEqual([2]);
  });

  it("should return an array with multiple duplicate elements when multiple duplicates are found", () => {
    const array = [1, 2, 3, 4, 5, 2, 6, 7, 8, 2, 9];
    expect(findDuplicates(array)).toEqual([2]);
  });

  it("should handle duplicate counts greater than 1", () => {
    const array = [1, 1, 2, 3, 4, 5, 2, 6, 7, 8, 2, 9];
    expect(findDuplicates(array)).toEqual([1, 2]);
  });
});

describe("flattenArray", () => {
  it("flattens nested arrays", () => {
    expect(flattenArray([1, [2, [3, 4], 5], 6])).toEqual([1, 2, 3, 4, 5, 6]);
  });
  it("handles empty array", () => {
    expect(flattenArray([])).toEqual([]);
  });
});

describe("duplicateCount", () => {
  it("counts duplicate characters", () => {
    expect(duplicateCount("aabbcdef")).toBe(2);
  });
  it("handles empty string", () => {
    expect(duplicateCount("")).toBe(0);
  });
});
describe("reverseWordsInSentence", () => {
  test("Reverse words in a sentence", () => {
    expect(reverseWordsInSentence("To jest przykładowe zdanie")).toEqual(
      "oT tsej ewodałkyzrp einadz"
    );
  });

  test("Reverse words in a sentence with punctuation", () => {
    expect(reverseWordsInSentence("Ala ma kota!")).toEqual("alA am atok");
  });

  test("Reverse words in a sentence with multiple spaces", () => {
    expect(reverseWordsInSentence("  Ala   ma  kota ")).toEqual("alA am atok");
  });

  test("Reverse words in a sentence with empty string", () => {
    expect(reverseWordsInSentence("")).toEqual("");
  });

  test("Reverse words in a sentence with single word", () => {
    expect(reverseWordsInSentence("Hello")).toEqual("olleH");
  });
});
describe("rotateArray", () => {
  test("rotates array by k elements to the right", () => {
    expect(rotateArray([1, 2, 3, 4, 5], 2)).toEqual([4, 5, 1, 2, 3]);
  });

  test("rotates array by k elements to the left", () => {
    expect(rotateArray([1, 2, 3, 4, 5], 3)).toEqual([3, 4, 5, 1, 2]);
  });

  test("handles rotation by 0 elements", () => {
    expect(rotateArray([1, 2, 3, 4, 5], 0)).toEqual([1, 2, 3, 4, 5]);
  });

  test("handles rotation by more elements than array length", () => {
    expect(rotateArray([1, 2, 3, 4, 5], 7)).toEqual([4, 5, 1, 2, 3]);
  });

  test("handles empty array", () => {
    expect(rotateArray([], 2)).toEqual([]);
  });

  test("handles k larger than array length", () => {
    expect(rotateArray([1, 2, 3, 4, 5], 6)).toEqual([5, 1, 2, 3, 4]);
  });
});
describe("findMinMax", () => {
  test("finds smallest and largest numbers in the array", () => {
    expect(findMinMax([1, 3, 5, 2, 4])).toEqual({ smallest: 1, largest: 5 });
  });

  test("finds smallest and largest numbers in the array with negative numbers", () => {
    expect(findMinMax([-3, -1, -5, -2, -4])).toEqual({
      smallest: -5,
      largest: -1,
    });
  });

  test("handles array with one element", () => {
    expect(findMinMax([7])).toEqual({ smallest: 7, largest: 7 });
  });

  test("handles array with repeated elements", () => {
    expect(findMinMax([3, 3, 3, 3])).toEqual({ smallest: 3, largest: 3 });
  });

  test("handles empty array", () => {
    expect(findMinMax([])).toBe("Array is empty");
  });
});

describe("firstNonRepeatingCharacter", () => {
  test("returns the first non-repeating character in a word", () => {
    expect(firstNonRepeatingCharacter("hello")).toBe("h");
  });

  test("handles word with all repeating characters", () => {
    expect(firstNonRepeatingCharacter("aaaaa")).toBeNull();
  });

  test("handles word with only one character", () => {
    expect(firstNonRepeatingCharacter("a")).toBe("a");
  });

  test("handles word with mixed repeating and non-repeating characters", () => {
    expect(firstNonRepeatingCharacter("abcdefgabc")).toBe("d");
  });

  test("handles word with spaces and punctuation", () => {
    expect(
      firstNonRepeatingCharacter(
        "a b c d e f g h i j k l m n o p q r s t u v w x y z"
      )
    ).toBe("a");
  });

  test("handles empty word", () => {
    expect(firstNonRepeatingCharacter("")).toBeNull();
  });
});

describe("findAllAnagrams", () => {
  test("finds all anagrams in the array", () => {
    expect(findAllAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"])).toEqual(
      [["eat", "tea", "ate"], ["tan", "nat"], ["bat"]]
    );
  });

  test("handles empty array", () => {
    expect(findAllAnagrams([])).toEqual([]);
  });

  test("handles array with one word", () => {
    expect(findAllAnagrams(["hello"])).toEqual([["hello"]]);
  });

  test("handles array with words that are not anagrams", () => {
    expect(findAllAnagrams(["hello", "world"])).toEqual([["hello"], ["world"]]);
  });
});

describe("fibonacci", () => {
  test("returns the correct fibonacci number for n = 0", () => {
    expect(fibonacci(0)).toBe(0);
  });

  test("returns the correct fibonacci number for n = 1", () => {
    expect(fibonacci(1)).toBe(1);
  });

  test("returns the correct fibonacci number for n = 2", () => {
    expect(fibonacci(2)).toBe(1);
  });

  test("returns the correct fibonacci number for n = 3", () => {
    expect(fibonacci(3)).toBe(2);
  });

  test("returns the correct fibonacci number for n = 4", () => {
    expect(fibonacci(4)).toBe(3);
  });

  test("returns the correct fibonacci number for n = 5", () => {
    expect(fibonacci(5)).toBe(5);
  });

  test("returns the correct fibonacci number for n = 10", () => {
    expect(fibonacci(10)).toBe(55);
  });

  test("returns the correct fibonacci number for n = 20", () => {
    expect(fibonacci(20)).toBe(6765);
  });

  test("returns the correct fibonacci number for n = 30", () => {
    expect(fibonacci(30)).toBe(832040);
  });
});

describe("noRepetition", () => {
  test("returns true for a string with no repeated characters", () => {
    expect(noRepetition("abcdefg")).toBe(true);
  });

  test("returns false for a string with repeated characters", () => {
    expect(noRepetition("hello")).toBe(false);
  });

  test("returns true for an empty string", () => {
    expect(noRepetition("")).toBe(true);
  });

  test("returns true for a single character string", () => {
    expect(noRepetition("a")).toBe(true);
  });
});

describe("longestSubstring", () => {
  test("returns the longest substring with no repeated characters", () => {
    expect(longestSubstring("abcabcbb")).toBe("abc");
  });

  test("returns the longest substring with no repeated characters", () => {
    expect(longestSubstring("bbbbb")).toBe("b");
  });

  test("returns the longest substring with no repeated characters", () => {
    expect(longestSubstring("pwwkew")).toBe("wke");
  });

  test("returns the longest substring with no repeated characters", () => {
    expect(longestSubstring("")).toBe("");
  });

  test("returns the longest substring with no repeated characters", () => {
    expect(longestSubstring("abcdefgh")).toBe("abcdefgh");
  });

  test("returns the longest substring with no repeated characters", () => {
    expect(longestSubstring("a")).toBe("a");
  });
});

describe("areAnagrams", () => {
  test("returns true for anagrams", () => {
    expect(areAnagrams("listen", "silent")).toBe(true);
  });

  test("returns true for anagrams with different cases", () => {
    expect(areAnagrams("Listen", "Silent")).toBe(true);
  });

  test("returns true for anagrams with extra spaces", () => {
    expect(areAnagrams("conversation", "voices rant on")).toBe(true);
  });

  test("returns true for anagrams with different punctuation", () => {
    expect(areAnagrams("debit card", "bad credit")).toBe(true);
  });

  test("returns false for non-anagrams", () => {
    expect(areAnagrams("hello", "world")).toBe(false);
  });

  test("returns false for strings with different lengths", () => {
    expect(areAnagrams("listen", "listens")).toBe(false);
  });

  test("returns true for empty strings", () => {
    expect(areAnagrams("", "")).toBe(true);
  });
});

describe("findIntersection", () => {
  test("returns common elements for arrays with common elements", () => {
    expect(findIntersection([1, 2, 3, 4, 5], [4, 5, 6, 7, 8])).toEqual([4, 5]);
  });

  test("returns an empty array for arrays with no common elements", () => {
    expect(findIntersection([1, 2, 3], [4, 5, 6])).toEqual([]);
  });

  test("returns common elements for arrays with duplicates", () => {
    expect(findIntersection([1, 2, 2, 3, 4], [2, 3, 3, 4, 5])).toEqual([
      2, 3, 4,
    ]);
  });

  test("returns common elements for arrays with different types", () => {
    expect(findIntersection([1, "two", 3, "four"], ["two", 3, "five"])).toEqual(
      ["two", 3]
    );
  });

  test("returns common elements for arrays with repeated elements", () => {
    expect(findIntersection([1, 2, 2, 3, 3], [2, 2, 3, 3, 4])).toEqual([2, 3]);
  });

  test("returns an empty array for empty arrays", () => {
    expect(findIntersection([], [])).toEqual([]);
  });

  test("returns an empty array when one array is empty", () => {
    expect(findIntersection([1, 2, 3], [])).toEqual([]);
  });
});

describe("findMostFrequent", () => {
  test("returns the most frequent element in the array", () => {
    expect(findMostFrequent([1, 2, 2, 3, 3, 3, 4, 4, 4, 4])).toBe(4);
  });

  test("returns the first most frequent element in case of a tie", () => {
    expect(findMostFrequent([1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 1])).toBe(4);
  });

  test("returns the most frequent element in case of negative numbers", () => {
    expect(findMostFrequent([-1, -2, -2, -3, -3, -3, -4, -4, -4, -4])).toBe(-4);
  });

  test("returns the most frequent element when it is a string", () => {
    expect(
      findMostFrequent(["apple", "banana", "banana", "banana", "cherry"])
    ).toBe("banana");
  });

  test("returns null for an empty array", () => {
    expect(findMostFrequent([])).toBeNull();
  });

  test("returns the only element for an array with one element", () => {
    expect(findMostFrequent([5])).toBe(5);
  });

  test("returns the most frequent element when all elements are the same", () => {
    expect(findMostFrequent([7, 7, 7, 7, 7])).toBe(7);
  });
});

describe("isPrimeNumber", () => {
  test("returns true for prime numbers", () => {
    expect(isPrimeNumber(2)).toBe(true);
    expect(isPrimeNumber(3)).toBe(true);
    expect(isPrimeNumber(5)).toBe(true);
    expect(isPrimeNumber(7)).toBe(true);
    expect(isPrimeNumber(11)).toBe(true);
    expect(isPrimeNumber(13)).toBe(true);
    expect(isPrimeNumber(17)).toBe(true);
    expect(isPrimeNumber(19)).toBe(true);
    expect(isPrimeNumber(23)).toBe(true);
    expect(isPrimeNumber(29)).toBe(true);
  });

  test("returns false for non-prime numbers", () => {
    expect(isPrimeNumber(1)).toBe(false);
    expect(isPrimeNumber(4)).toBe(false);
    expect(isPrimeNumber(6)).toBe(false);
    expect(isPrimeNumber(8)).toBe(false);
    expect(isPrimeNumber(9)).toBe(false);
    expect(isPrimeNumber(10)).toBe(false);
    expect(isPrimeNumber(12)).toBe(false);
    expect(isPrimeNumber(14)).toBe(false);
    expect(isPrimeNumber(15)).toBe(false);
    expect(isPrimeNumber(20)).toBe(false);
  });

  test("returns false for zero", () => {
    expect(isPrimeNumber(0)).toBe(false);
  });
});

describe("reverseWordsInSentence", () => {
  test("reverses words in a sentence with multiple words", () => {
    expect(reverseWordsInSentence("Hello world")).toBe("olleH dlrow");
  });

  test("reverses words in a sentence with punctuation", () => {
    expect(reverseWordsInSentence("Hello, world!")).toBe("olleH dlrow");
  });

  test("reverses words in a sentence with multiple spaces between words", () => {
    expect(reverseWordsInSentence("Hello  world")).toBe("olleH dlrow");
  });

  test("reverses words in a sentence with single-letter words", () => {
    expect(reverseWordsInSentence("a b c d e f g")).toBe("a b c d e f g");
  });

  test("reverses words in an empty sentence", () => {
    expect(reverseWordsInSentence("")).toBe("");
  });

  test("reverses words in a sentence with a single word", () => {
    expect(reverseWordsInSentence("Hello")).toBe("olleH");
  });
});

describe("findPairsWithSum", () => {
  test("returns pairs with sum equal to target", () => {
    expect(findPairsWithSum([1, 2, 3, 4, 5], 7)).toEqual([
      [3, 4],
      [2, 5],
    ]);
  });

  test("returns empty array when array is empty", () => {
    expect(findPairsWithSum([], 5)).toEqual([]);
  });

  test("returns empty array when target is zero", () => {
    expect(findPairsWithSum([1, 2, 3, 4, 5], 0)).toEqual([]);
  });

  test("returns empty array when array has only one element", () => {
    expect(findPairsWithSum([5], 5)).toEqual([]);
  });
});

describe("isPermutationPalindrome", () => {
  test("returns true for palindrome permutation", () => {
    expect(isPermutationPalindrome("civic")).toBe(true);
    expect(isPermutationPalindrome("ivicc")).toBe(true);
    expect(isPermutationPalindrome("racecar")).toBe(true);
    expect(isPermutationPalindrome("radar")).toBe(true);
  });

  test("returns true for single-character string", () => {
    expect(isPermutationPalindrome("a")).toBe(true);
  });

  test("returns true for empty string", () => {
    expect(isPermutationPalindrome("")).toBe(true);
  });

  test("returns true for palindrome permutation with spaces", () => {
    expect(isPermutationPalindrome("taco cat")).toBe(true);
    expect(isPermutationPalindrome("a man a plan a canal panama")).toBe(true);
  });

  test("returns false for non-palindrome permutation", () => {
    expect(isPermutationPalindrome("hello")).toBe(false);
    expect(isPermutationPalindrome("world")).toBe(false);
    expect(isPermutationPalindrome("banana")).toBe(false);
    expect(isPermutationPalindrome("apple")).toBe(false);
  });

  test("returns false for non-palindrome permutation with spaces", () => {
    expect(isPermutationPalindrome("this is not a palindrome")).toBe(false);
    expect(isPermutationPalindrome("no sir away! a papaya war is on")).toBe(
      false
    );
  });
});

describe("isPangram", () => {
  test("returns true for pangram strings", () => {
    expect(isPangram("The quick brown fox jumps over the lazy dog")).toBe(true);
    expect(isPangram("Pack my box with five dozen liquor jugs")).toBe(true);
    expect(isPangram("Jackdaws love my big sphinx of quartz")).toBe(true);
  });

  test("returns false for non-pangram strings", () => {
    expect(isPangram("Hello world")).toBe(false);
    expect(isPangram("Lorem ipsum dolor sit amet")).toBe(false);
    expect(isPangram("The quick brown fox jumps over the fence")).toBe(false);
  });

  test("returns true for pangram strings with uppercase letters", () => {
    expect(isPangram("The Quick Brown Fox Jumps Over The Lazy Dog")).toBe(true);
    expect(isPangram("Pack My Box With Five Dozen Liquor Jugs")).toBe(true);
  });

  test("returns true for pangram strings with punctuation and spaces", () => {
    expect(isPangram("The quick, brown fox jumps over the lazy dog.")).toBe(
      true
    );
    expect(isPangram("Pack my box with five dozen liquor jugs!")).toBe(true);
    expect(isPangram("Jackdaws, love my big sphinx of quartz.")).toBe(true);
  });

  test("returns false for empty string", () => {
    expect(isPangram("")).toBe(false);
  });

  test("returns false for single-letter string", () => {
    expect(isPangram("a")).toBe(false);
    expect(isPangram("z")).toBe(false);
  });

  test("returns false for non-alphabetic characters only", () => {
    expect(isPangram("1234567890")).toBe(false);
    expect(isPangram(",.;:?!@#$%^&*()")).toBe(false);
  });
});

describe("findLongestNonDecraisingSubsequence", () => {
  test("returns the longest increasing subsequence for a given array", () => {
    expect(findLongestNonDecraisingSubsequence([3, 10, 2, 1, 20])).toEqual([
      3, 10, 20,
    ]);
    expect(
      findLongestNonDecraisingSubsequence([10, 22, 9, 33, 21, 50, 41, 60, 80])
    ).toEqual([10, 22, 33, 50, 60, 80]);
    expect(findLongestNonDecraisingSubsequence([3, 4, -1, 0, 6, 2, 3])).toEqual(
      [-1, 0, 2, 3]
    );
  });

  test("returns an empty array for an empty input array", () => {
    expect(findLongestNonDecraisingSubsequence([])).toEqual([]);
  });

  test("returns the longest increasing subsequence when all elements are the same", () => {
    expect(findLongestNonDecraisingSubsequence([5, 5, 5, 5, 5])).toEqual([
      5, 5, 5, 5, 5,
    ]);
  });

  test("returns the longest increasing subsequence when all elements are in descending order", () => {
    expect(findLongestNonDecraisingSubsequence([5, 4, 3, 2, 1])).toEqual([5]);
  });

  test("returns the longest increasing subsequence when all elements are in ascending order", () => {
    expect(findLongestNonDecraisingSubsequence([1, 2, 3, 4, 5])).toEqual([
      1, 2, 3, 4, 5,
    ]);
  });

  test("returns the longest increasing subsequence when there are negative numbers", () => {
    expect(findLongestNonDecraisingSubsequence([-1, -2, -3, -4, -5])).toEqual([
      -1,
    ]);
    expect(findLongestNonDecraisingSubsequence([-1, 0, -2, 1, -3, 2])).toEqual([
      -1, 0, 1, 2,
    ]);
  });
});

describe("findMaxProductOfTwo function", () => {
  test("should return correct result for simple case", () => {
    expect(findMaxProductOfTwo([2, 3, 4, 5])).toBe(20);
  });

  test("should handle negative numbers", () => {
    expect(findMaxProductOfTwo([-2, -3, -4, -5])).toBe(20);
  });

  test("should handle array with zeros", () => {
    expect(findMaxProductOfTwo([0, 1, 2, 3])).toBe(6);
  });

  test("should handle array with only one element", () => {
    expect(findMaxProductOfTwo([5])).toBeUndefined();
  });

  test("should handle array with two elements", () => {
    expect(findMaxProductOfTwo([5, 10])).toBe(50);
  });

  test("should handle large numbers", () => {
    expect(findMaxProductOfTwo([1000, 2000, 3000, 4000])).toBe(12000000);
  });

  test("should handle array with a large number of elements", () => {
    expect(findMaxProductOfTwo([2, 3, 4, 5, 6, 7, 8, 9, 10])).toBe(90);
  });
});

test("Returns correct result for a simple array", () => {
  expect(findLongestIncreasingSubsequence([3, 1, 5, 2, 6, 4, 9])).toEqual([
    3, 5, 6, 9,
  ]);
});

test("Returns empty array for empty input array", () => {
  expect(findLongestIncreasingSubsequence([])).toEqual([]);
});

test("Returns correct result for an array with one element", () => {
  expect(findLongestIncreasingSubsequence([5])).toEqual([5]);
});

test("Handles negative numbers", () => {
  expect(findLongestIncreasingSubsequence([-2, -1, 0, 1, 2])).toEqual([
    -2, -1, 0, 1, 2,
  ]);
});

test("Handles array in reverse order", () => {
  expect(findLongestIncreasingSubsequence([5, 4, 3, 2, 1])).toEqual([5]);
});

test("Handles array with all elements same", () => {
  expect(findLongestIncreasingSubsequence([1, 1, 1, 1, 1])).toEqual([1]);
});

test("Returns true for correctly paired brackets", () => {
  expect(checkBrackets("()")).toBe(true);
  expect(checkBrackets("()[]{}")).toBe(true);
  expect(checkBrackets("{[()]}")).toBe(true);
  expect(checkBrackets("[{()}]")).toBe(true);
});

test("Returns false for incorrectly paired brackets", () => {
  expect(checkBrackets("(]")).toBe(false);
  expect(checkBrackets("([)]")).toBe(false);
  expect(checkBrackets("{[}]")).toBe(false);
});

test("Handles other characters correctly", () => {
  expect(checkBrackets("abc")).toBe(true);
  expect(checkBrackets("")).toBe(true);
  expect(checkBrackets("a(b)c[d]e{f}")).toBe(true);
});

test("Handles nested brackets correctly", () => {
  expect(checkBrackets("([{}])")).toBe(true);
  expect(checkBrackets("([{})")).toBe(false);
  expect(checkBrackets("[{()}]")).toBe(true);
});

test("Handles edge cases correctly", () => {
  expect(checkBrackets("((")).toBe(false);
  expect(checkBrackets("))")).toBe(false);
});

test("Analyzes text correctly", () => {
  const text =
    "To jest przykładowy tekst. Jest to tekst do analizy, który zawiera kilka słów. To jest prosty test.";
  const analysisResult = analyzeText(text);

  expect(analysisResult.totalWords).toBe(17);
  expect(analysisResult.uniqueWords).toBe(12);
  expect(analysisResult.totalSentences).toBe(3);
  expect(analysisResult.wordFrequency).toEqual({
    to: 3,
    jest: 3,
    przykładowy: 1,
    tekst: 2,
    do: 1,
    analizy: 1,
    który: 1,
    zawiera: 1,
    kilka: 1,
    słów: 1,
    prosty: 1,
    test: 1,
  });
});

test("Handles text with no sentences correctly", () => {
  const text = "Tekst bez kropek, bez zdań";
  const analysisResult = analyzeText(text);

  expect(analysisResult.totalWords).toBe(5);
  expect(analysisResult.uniqueWords).toBe(4);
  expect(analysisResult.wordFrequency).toEqual({
    tekst: 1,
    bez: 2,
    kropek: 1,
    zdań: 1,
  });
});

test("Handles empty text correctly", () => {
  const text = "";
  const analysisResult = analyzeText(text);

  expect(analysisResult.totalWords).toBe(0);
  expect(analysisResult.uniqueWords).toBe(0);
  expect(analysisResult.totalSentences).toBe(0);
  expect(analysisResult.wordFrequency).toEqual({});
});

describe("Quick Sort", () => {
  // Test dla pustej tablicy
  it("should return an empty array for an empty array input", () => {
    expect(quickSort([])).toEqual([]);
  });

  // Test dla tablicy z jednym elementem
  it("should return the same array for an array with one element", () => {
    expect(quickSort([5])).toEqual([5]);
  });

  // Test dla tablicy z kilkoma elementami
  it("should correctly sort an array with multiple elements", () => {
    expect(quickSort([7, 2, 1, 6, 8, 5, 3, 4])).toEqual([
      1, 2, 3, 4, 5, 6, 7, 8,
    ]);
  });

  // Test dla tablicy zawierającej duplikaty
  it("should correctly sort an array with duplicate elements", () => {
    expect(quickSort([5, 2, 9, 5, 2, 1])).toEqual([1, 2, 2, 5, 5, 9]);
  });

  // Test dla tablicy posortowanej malejąco
  it("should correctly sort an array sorted in descending order", () => {
    expect(quickSort([9, 7, 5, 3, 1])).toEqual([1, 3, 5, 7, 9]);
  });

  // Test dla tablicy zawierającej elementy w odwrotnej kolejności
  it("should correctly sort an array with elements in reverse order", () => {
    expect(quickSort([3, 2, 1])).toEqual([1, 2, 3]);
  });
});

describe("findMissingNumber2", () => {
  it("should return the missing number when it is in the middle of the array", () => {
    expect(findMissingNumber2([1, 2, 4, 5, 6])).toBe(3);
  });

  it("should return the missing number when it is at the beginning of the array", () => {
    expect(findMissingNumber2([1, 3, 4, 5])).toBe(2);
  });

  it("should return the missing number when it is at the end of the array", () => {
    expect(findMissingNumber2([2, 3, 4, 5])).toBe(1);
  });

  it("should return the missing number when it is the last element of the array", () => {
    expect(findMissingNumber2([1, 2, 3, 4])).toBe(5);
  });
});

describe("capitalizeWords", () => {
  it("should capitalize each word in the sentence", () => {
    expect(capitalizeWords("hello world")).toBe("Hello World");
    expect(capitalizeWords("lorem ipsum dolor sit amet")).toBe(
      "Lorem Ipsum Dolor Sit Amet"
    );
  });

  it("should handle empty string", () => {
    expect(capitalizeWords("")).toBe("");
  });

  it("should handle single-word sentence", () => {
    expect(capitalizeWords("javascript")).toBe("Javascript");
  });

  it("should handle sentence with numbers and special characters", () => {
    expect(capitalizeWords("abc 123 xyz !@#")).toBe("Abc 123 Xyz !@#");
  });
});

describe("validateEmail", () => {
  it("should return true for a valid email address", () => {
    expect(validateEmail("john@example.com")).toBe(true);
    expect(validateEmail("jane.doe@example.com")).toBe(true);
    expect(validateEmail("john_doe@example.com")).toBe(true);
    expect(validateEmail("john.doe123@example.com")).toBe(true);
  });

  it('should return false for an email address without "@"', () => {
    expect(validateEmail("example.com")).toBe(false);
    expect(validateEmail("johnexample.com")).toBe(false);
    expect(validateEmail("johnexamplecom")).toBe(false);
  });

  it('should return false for an email address without "." after "@"', () => {
    expect(validateEmail("john@examplecom")).toBe(false);
    expect(validateEmail("john@example")).toBe(false);
    expect(validateEmail("john@example.")).toBe(false);
  });

  it("should return false for an email address with space", () => {
    expect(validateEmail("john doe@example.com")).toBe(false);
    expect(validateEmail("johndoe @example.com")).toBe(false);
    expect(validateEmail("johndoe@ example.com")).toBe(false);
  });

  it("should return false for an email address with domain without letters", () => {
    expect(validateEmail("john@123.com")).toBe(false);
    expect(validateEmail("john@1234567890.com")).toBe(false);
    expect(validateEmail("john@com")).toBe(false);
  });
});

describe("mergeArrays", () => {
  it("should merge two sorted arrays into one sorted array", () => {
    expect(mergeArrays2([1, 3, 5], [2, 4, 6])).toEqual([1, 2, 3, 4, 5, 6]);
    expect(mergeArrays2([-1, 0, 4], [1, 2, 3])).toEqual([-1, 0, 1, 2, 3, 4]);
    expect(mergeArrays2([], [2, 4, 6])).toEqual([2, 4, 6]);
    expect(mergeArrays2([1, 3, 5], [])).toEqual([1, 3, 5]);
    expect(mergeArrays2([], [])).toEqual([]);
  });

  it("should handle arrays with duplicate elements", () => {
    expect(mergeArrays2([1, 2, 3], [2, 3, 4])).toEqual([1, 2, 2, 3, 3, 4]);
    expect(mergeArrays2([1, 1, 2], [2, 3, 3])).toEqual([1, 1, 2, 2, 3, 3]);
  });

  it("should handle arrays with negative numbers", () => {
    expect(mergeArrays2([-3, -2, -1], [-4, -3, -2])).toEqual([
      -4, -3, -3, -2, -2, -1,
    ]);
  });
});

describe("binarySearch", () => {
  it("should return the index of the target element if found in the sorted array", () => {
    expect(binarySearch([1, 2, 3, 4, 5], 4)).toBe(3);
    expect(binarySearch([1, 2, 3, 4, 5], 6)).toBe(-1);
    expect(binarySearch(["a", "b", "c", "d", "e"], "c")).toBe(2);
    expect(
      binarySearch(["cat", "dog", "elephant", "lion", "tiger"], "dog")
    ).toBe(1);
    expect(binarySearch([10, 20, 30, 40, 50], 30)).toBe(2);
  });

  it("should return -1 if the target element is not found in the array", () => {
    expect(binarySearch([1, 2, 3, 4, 5], 6)).toBe(-1);
    expect(binarySearch(["a", "b", "c", "d", "e"], "f")).toBe(-1);
    expect(
      binarySearch(["cat", "dog", "elephant", "lion", "tiger"], "zebra")
    ).toBe(-1);
  });

  it("should return -1 for an empty array", () => {
    expect(binarySearch([], 5)).toBe(-1);
  });
});

describe("findMaxConsecutiveOnes", () => {
  it("should return the length of the longest sequence of consecutive ones", () => {
    expect(findMaxConsecutiveOnes([1, 1, 0, 1, 1, 1])).toBe(3);
    expect(findMaxConsecutiveOnes([0, 0, 0, 1, 1, 0, 1, 1, 1])).toBe(3);
    expect(findMaxConsecutiveOnes([1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1])).toBe(4);
    expect(findMaxConsecutiveOnes([1, 0, 0, 0, 0])).toBe(1);
    expect(findMaxConsecutiveOnes([])).toBe(0);
  });

  it("should return 0 if there are no ones in the array", () => {
    expect(findMaxConsecutiveOnes([0, 0, 0, 0])).toBe(0);
    expect(findMaxConsecutiveOnes([0])).toBe(0);
    expect(findMaxConsecutiveOnes([])).toBe(0);
  });

  it("should return 1 if there is only one one in the array", () => {
    expect(findMaxConsecutiveOnes([1])).toBe(1);
    expect(findMaxConsecutiveOnes([0, 1])).toBe(1);
    expect(findMaxConsecutiveOnes([0, 0, 1])).toBe(1);
  });

  it("should handle arrays with alternating ones and zeros", () => {
    expect(findMaxConsecutiveOnes([1, 0, 1, 0, 1])).toBe(1);
    expect(findMaxConsecutiveOnes([0, 1, 0, 1, 0])).toBe(1);
    expect(findMaxConsecutiveOnes([1, 1, 0, 0, 1, 1])).toBe(2);
  });
});

describe("validatePassword", () => {
  it("should return true for a valid password", () => {
    expect(validatePassword("Abc123!@")).toBe(true);
    expect(validatePassword("Password1!")).toBe(true);
    expect(validatePassword("AbCdEfGh123!@#")).toBe(true);
  });

  it("should return false for a password shorter than 8 characters", () => {
    expect(validatePassword("Abc12!")).toBe(false);
    expect(validatePassword("Pass1!")).toBe(false);
    expect(validatePassword("AbC123!")).toBe(false);
  });

  it("should return false for a password without a lowercase letter", () => {
    expect(validatePassword("ABC123!@")).toBe(false);
    expect(validatePassword("PASSWORD1!")).toBe(false);
    expect(validatePassword("ABCD123!@#")).toBe(false);
  });

  it("should return false for a password without an uppercase letter", () => {
    expect(validatePassword("abc123!@")).toBe(false);
    expect(validatePassword("password1!")).toBe(false);
    expect(validatePassword("abcd123!@#")).toBe(false);
  });

  it("should return false for a password without a digit", () => {
    expect(validatePassword("Abcdefg!@")).toBe(false);
    expect(validatePassword("Password!@")).toBe(false);
    expect(validatePassword("Abcdefg!@#")).toBe(false);
  });

  it("should return false for a password without a special character", () => {
    expect(validatePassword("Abc12345")).toBe(false);
    expect(validatePassword("Password123")).toBe(false);
    expect(validatePassword("Abcdefg123")).toBe(false);
  });
});

describe("findDuplicate", () => {
  it("should return the duplicated number in the array", () => {
    expect(findDuplicate([1, 3, 4, 2, 2])).toBe(2);
    expect(findDuplicate([3, 1, 3, 4, 2])).toBe(3);
    expect(findDuplicate([1, 1])).toBe(1);
    expect(findDuplicate([1, 2, 3, 4, 5, 5])).toBe(5);
    expect(findDuplicate([2, 2, 3, 4, 5])).toBe(2);
  });

  it("should handle edge cases with only two elements in the array", () => {
    expect(findDuplicate([1, 1])).toBe(1);
    expect(findDuplicate([2, 2])).toBe(2);
  });

  it("should handle edge cases with large arrays", () => {
    const arr = [];
    for (let i = 1; i <= 1000; i++) {
      arr.push(i);
    }
    arr.push(999); // Adding duplicate 999
    expect(findDuplicate(arr)).toBe(999);
  });
});

describe("findPeakElement", () => {
  it("should return the index of a peak element in the array", () => {
    expect(findPeakElement([1, 2, 3, 1])).toBe(2);
    expect([1, 2].includes(findPeakElement([1, 2, 1, 3, 5, 6, 4]))).toBe(true);
    expect(findPeakElement([1, 2, 3, 4, 5])).toBe(4);
    expect(findPeakElement([5, 4, 3, 2, 1])).toBe(0);
  });

  it("should return any index if there are multiple peak elements", () => {
    const peakIndex = findPeakElement([1, 1, 1, 1]);
    expect([0, 3].includes(peakIndex)).toBe(true);
  });

  it("should handle edge cases with small arrays", () => {
    expect(findPeakElement([1])).toBe(0);
    expect(findPeakElement([1, 2])).toBe(1);
    expect(findPeakElement([2, 1])).toBe(0);
  });

  it("should handle edge cases with large arrays", () => {
    const arr = [];
    for (let i = 0; i < 1000; i++) {
      arr.push(i);
    }
    arr[500] = 999; // Setting a peak element
    expect(findPeakElement(arr)).toBe(500);
  });
});

describe("maximumSubarraySum", () => {
  it("should return the maximum sum of subarray of length k", () => {
    expect(maximumSubarraySum([100, 200, 300, 400], 2)).toBe(700);
    expect(maximumSubarraySum([1, 4, 2, 10, 23, 3, 1, 0, 20], 4)).toBe(57);
    expect(maximumSubarraySum([-3, 4, 0, -2, 6, -1], 2)).toBe(10);
    expect(maximumSubarraySum([3, 5, 1, 7, 6, 2, 9, 4], 3)).toBe(22);
  });

  it("should return 0 if the array is empty", () => {
    expect(maximumSubarraySum([], 3)).toBe(0);
  });

  it("should handle edge cases with k equal to array length", () => {
    expect(maximumSubarraySum([1, 2, 3, 4], 4)).toBe(10);
  });

  it("should handle edge cases with k equal to 1", () => {
    expect(maximumSubarraySum([1, 2, 3, 4], 1)).toBe(4);
    expect(maximumSubarraySum([-1, -2, -3, -4], 1)).toBe(-1);
  });

  it("should handle edge cases with negative numbers", () => {
    expect(maximumSubarraySum([-1, -2, -3, -4], 2)).toBe(-3);
    expect(maximumSubarraySum([-1, -2, -3, -4], 3)).toBe(-6);
  });
});

describe("sortByStringLength", () => {
  it("should sort array of strings by length in ascending order", () => {
    expect(sortByStringLength(["kot", "pies", "rybki", "ptak"])).toEqual([
      "kot",
      "pies",
      "ptak",
      "rybki",
    ]);
    expect(
      sortByStringLength(["jabłko", "gruszka", "banan", "śliwka"])
    ).toEqual(["banan", "jabłko", "śliwka", "gruszka"]);
    expect(sortByStringLength(["a", "bb", "ccc", "dddd"])).toEqual([
      "a",
      "bb",
      "ccc",
      "dddd",
    ]);
    expect(sortByStringLength(["", "", "", ""])).toEqual(["", "", "", ""]);
    expect(sortByStringLength([])).toEqual([]);
  });

  it("should return the same array if it contains only one element", () => {
    expect(sortByStringLength(["jabłko"])).toEqual(["jabłko"]);
  });

  it("should return the same array if it contains only empty strings", () => {
    expect(sortByStringLength(["", "", "", ""])).toEqual(["", "", "", ""]);
  });

  it("should handle edge cases with very long strings", () => {
    const longStrings = ["a".repeat(100), "b".repeat(200), "c".repeat(300)];
    expect(sortByStringLength(longStrings)).toEqual([
      "a".repeat(100),
      "b".repeat(200),
      "c".repeat(300),
    ]);
  });
});

// describe('countTriplets', () => {
//   it('should return empty array if no triplets are found', () => {
//     expect(countTriplets([1, 2, 3, 4, 5], 10)).toEqual([]);
//   });

//   it('should return correct triplets for valid input', () => {
//     expect(countTriplets([1, 2, 3, 4, 5, 6, 7], 12)).toEqual([[1, 5, 6], [2, 4, 6], [3, 4, 5]]);
//     expect(countTriplets([3, 1, 2, 5, 4, 6], 12)).toEqual([[1, 5, 6], [2, 4, 6], [3, 4, 5]]);
//     expect(countTriplets([1, 1, 1, 1, 1], 3)).toEqual([[1, 1, 1]]);
//   });

//   it('should handle edge cases', () => {
//     expect(countTriplets([], 5)).toEqual([]);
//     expect(countTriplets([1, 2, 3], 6)).toEqual([1,2,3]);
//     expect(countTriplets([1, 2, 3, 4], 10)).toEqual([]);
//   });

//   it('should handle negative numbers and zero correctly', () => {
//     expect(countTriplets([-1, 0, 1, 2, 3], 2)).toEqual([[-1, 0, 3]]);
//     expect(countTriplets([-3, -2, -1, 0, 1, 2, 3], 0)).toEqual([[-3, 1, 2], [-2, 0, 2], [-1, 1, 0]]);
//   });
// });

describe("findCommonElements", () => {
  it("should return an array of common elements", () => {
    expect(findCommonElements([1, 2, 3, 4, 5], [4, 5, 6, 7, 8])).toEqual([
      4, 5,
    ]);
    expect(
      findCommonElements([1, 2, 2, 3, 4, 5], [2, 2, 3, 3, 4, 4, 5, 6])
    ).toEqual([2, 3, 4, 5]);
    expect(findCommonElements([1, 3, 5, 7, 9], [2, 4, 6, 8, 10])).toEqual([]);
    expect(findCommonElements([1, 1, 1, 1, 1], [1, 1, 1, 1, 1])).toEqual([1]);
    expect(findCommonElements([], [1, 2, 3])).toEqual([]);
  });

  it("should return an empty array if no common elements are found", () => {
    expect(findCommonElements([1, 2, 3], [4, 5, 6])).toEqual([]);
    expect(findCommonElements([1, 2, 3], [])).toEqual([]);
    expect(findCommonElements([], [])).toEqual([]);
  });

  it("should handle duplicate elements correctly", () => {
    expect(findCommonElements([1, 1, 2, 2, 3], [1, 2, 2, 3, 4])).toEqual([
      1, 2, 3,
    ]);
    expect(findCommonElements([1, 1, 2, 2, 3], [1, 1, 2, 2, 3])).toEqual([
      1, 2, 3,
    ]);
  });

  it("should handle negative numbers correctly", () => {
    expect(
      findCommonElements([-5, -3, -1, 0, 1, 3, 5], [-4, -2, 0, 2, 4])
    ).toEqual([0]);
    expect(
      findCommonElements([-5, -3, -1, 1, 3, 5], [-6, -4, -2, 0, 2, 4])
    ).toEqual([]);
  });
});

describe("findUniqueElements", () => {
  it("should return an array of unique elements", () => {
    expect(findUniqueElements([1, 2, 3, 4, 5])).toEqual([1, 2, 3, 4, 5]);
    expect(findUniqueElements([1, 2, 2, 3, 4, 4, 5])).toEqual([1, 3, 5]);
    expect(
      findUniqueElements(["apple", "banana", "apple", "orange", "banana"])
    ).toEqual(["orange"]);
    expect(findUniqueElements([true, false, true, true, false])).toEqual([]);
    expect(findUniqueElements([])).toEqual([]);
  });

  it("should handle empty arrays correctly", () => {
    expect(findUniqueElements([])).toEqual([]);
  });

  it("should return the same array if all elements are unique", () => {
    expect(findUniqueElements([1, 2, 3, 4, 5])).toEqual([1, 2, 3, 4, 5]);
    expect(findUniqueElements(["apple", "banana", "orange"])).toEqual([
      "apple",
      "banana",
      "orange",
    ]);
    expect(findUniqueElements([true, false])).toEqual([true, false]);
  });

  it("should return an empty array if input array is empty", () => {
    expect(findUniqueElements([])).toEqual([]);
  });

  it("should handle arrays with only one element correctly", () => {
    expect(findUniqueElements([1])).toEqual([1]);
    expect(findUniqueElements(["apple"])).toEqual(["apple"]);
    expect(findUniqueElements([true])).toEqual([true]);
  });
});

describe("mergeSort", () => {
  it("should sort an array of numbers in ascending order", () => {
    expect(mergeSort([4, 3, 6, 2, 8, 1, 5, 7])).toEqual([
      1, 2, 3, 4, 5, 6, 7, 8,
    ]);
    expect(mergeSort([9, 5, 7, 3, 2, 8, 1, 4, 6])).toEqual([
      1, 2, 3, 4, 5, 6, 7, 8, 9,
    ]);
    expect(mergeSort([5, 4, 3, 2, 1])).toEqual([1, 2, 3, 4, 5]);
    expect(mergeSort([])).toEqual([]);
    expect(mergeSort([1])).toEqual([1]);
    expect(mergeSort([2, 1])).toEqual([1, 2]);
    expect(mergeSort([1, 2, 3, 4, 5])).toEqual([1, 2, 3, 4, 5]);
  });

  it("should return an empty array when input array is empty", () => {
    expect(mergeSort([])).toEqual([]);
  });

  it("should return the same array when input array has only one element", () => {
    expect(mergeSort([1])).toEqual([1]);
  });

  it("should sort an array of strings in alphabetical order", () => {
    expect(
      mergeSort(["banana", "apple", "orange", "grape", "pineapple"])
    ).toEqual(["apple", "banana", "grape", "orange", "pineapple"]);
  });
});

describe("maxDiff", () => {
  it("should return 0 for an empty array", () => {
    expect(maxDiff([])).toEqual(0);
  });

  it("should return 0 for an array with a single element", () => {
    expect(maxDiff([5])).toEqual(0);
  });

  it("should return difference between the max and min values in the array", () => {
    expect(maxDiff([1, 2, 3, 4, 5])).toEqual(4);
    expect(maxDiff([-10, 0, 10, 20])).toEqual(30);
    expect(maxDiff([5, 5, 5, 5, 5])).toEqual(0); // Test for all equal values
  });

  it("should work with negative numbers", () => {
    expect(maxDiff([-5, -3, -1, 0, 2])).toEqual(7);
  });

  it("should work with floating-point numbers", () => {
    expect(maxDiff([1.5, 2.5, 3.5, 4.5])).toEqual(3);
  });
});

describe("removeDuplicates", () => {
  it("should remove duplicates and preserve the order of elements", () => {
    expect(removeDuplicates([1, 2, 3, 1, 2, 4, 5, 6, 4])).toEqual([
      1, 2, 3, 4, 5, 6,
    ]);
  });

  it("should remove duplicates when all elements are the same", () => {
    expect(removeDuplicates([5, 5, 5, 5, 5])).toEqual([5]);
  });

  it("should return an empty array if the input array is empty", () => {
    expect(removeDuplicates([])).toEqual([]);
  });

  it("should return the same array if there are no duplicates", () => {
    expect(removeDuplicates([1, 2, 3, 4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  });

  it("should handle arrays with a single element", () => {
    expect(removeDuplicates([5])).toEqual([5]);
  });
});

describe("isIsogram", () => {
  it("should return true for a word without repeating letters", () => {
    expect(isIsogram("algorithm")).toBe(true);
    expect(isIsogram("world")).toBe(true);
  });

  it("should return false for a word with repeating letters", () => {
    expect(isIsogram("javascript")).toBe(false);
    expect(isIsogram("hello")).toBe(false);
    expect(isIsogram("programming")).toBe(false);
    expect(isIsogram("testing")).toBe(false);
  });

  it("should handle uppercase and lowercase letters correctly", () => {
    expect(isIsogram("JavaScript")).toBe(false);
    expect(isIsogram("AlGorItHm")).toBe(true);
  });

  it("should return true for an empty string", () => {
    expect(isIsogram("")).toBe(true);
  });
});

describe("findMaxDifference", () => {
  it("should return 0 for an empty array", () => {
    expect(findMaxDifference([])).toBeNull();
  });

  it("should return 0 for an array with only one element", () => {
    expect(findMaxDifference([5])).toBe(0);
  });

  it("should return the correct max difference for an array with multiple elements", () => {
    expect(findMaxDifference([1, 9, 3, 5, 2, 8])).toBe(8);
    expect(findMaxDifference([10, 7, 5, 3, 1])).toBe(9);
    expect(findMaxDifference([1, 2, 3, 4, 5])).toBe(4);
  });

  it("should handle arrays with duplicate elements", () => {
    expect(findMaxDifference([1, 1, 1, 1])).toBe(0); // same elements, max difference is 0
    expect(findMaxDifference([1, 1, 2, 2, 3, 3])).toBe(2); // max difference is between 1 and 3
  });
});

describe("maxProductOfTwo", () => {
  it("should return null for an empty array", () => {
    expect(maxProductOfTwo([])).toBeNull();
  });

  it("should return null for an array with only one element", () => {
    expect(maxProductOfTwo([5])).toBeNull();
  });

  it("should return the correct max product for an array with multiple elements", () => {
    expect(maxProductOfTwo([2, 3, 4, 5])).toBe(20); // 4 * 5 = 20
    expect(maxProductOfTwo([-10, -3, 1, 5, -6])).toBe(60); // (-10) * (-6) = 60
    expect(maxProductOfTwo([1, 2, 3, 4, 5])).toBe(20); // 4 * 5 = 20
  });

  it("should handle arrays with duplicate elements", () => {
    expect(maxProductOfTwo([1, 1, 2, 2])).toBe(4); // 2 * 2 = 4
    expect(maxProductOfTwo([3, 3, 3, 3])).toBe(9); // 3 * 3 = 9
  });
});
describe("hasZeroSumTriplets", () => {
  it("should return true if there is a triplet summing to zero", () => {
    expect(hasZeroSumTriplets([-1, 0, 1, 2, -1, -4])).toBe(true);
    expect(hasZeroSumTriplets([0, 0, 0])).toBe(true);
  });

  it("should return false if there is no triplet summing to zero", () => {
    expect(hasZeroSumTriplets([1, 2, 3])).toBe(false);
    expect(hasZeroSumTriplets([-1, 2, 3])).toBe(false);
  });

  it("should handle edge cases with empty array or single element", () => {
    expect(hasZeroSumTriplets([])).toBe(false);
    expect(hasZeroSumTriplets([5])).toBe(false);
  });
});

describe("isPerfectNumber", () => {
  test("Test for perfect number: 6", () => {
    expect(isPerfectNumber(6)).toBe(true);
  });

  test("Test for perfect number: 28", () => {
    expect(isPerfectNumber(28)).toBe(true);
  });

  test("Test for non-perfect number: 12", () => {
    expect(isPerfectNumber(12)).toBe(false);
  });

  test("Test for non-perfect number: 27", () => {
    expect(isPerfectNumber(27)).toBe(false);
  });

  test("Test for edge case: 1", () => {
    expect(isPerfectNumber(1)).toBe(false);
  });
});

describe("findShortestPath", () => {
  const graph = {
    A: ["B", "C"],
    B: ["A", "C", "D"],
    C: ["A", "B", "D"],
    D: ["B", "C"],
  };

  test("finds shortest path between two vertices", () => {
    expect(findShortestPath(graph, "A", "D")).toEqual(["A", "B", "D"]);
  });

  test("returns null if no path is found", () => {
    expect(findShortestPath(graph, "A", "E")).toBeNull();
  });

  const graph0 = {
    A: ["B", "C"],
    B: ["A", "C", "D"],
    C: ["A", "B", "D", "E"],
    D: ["B", "C", "F"],
    E: ["C", "F"],
    F: ["D", "E", "G"],
    G: ["F"],
  };

  test("finds shortest path between A and G", () => {
    expect(findShortestPath(graph0, "A", "G")).toEqual([
      "A",
      "B",
      "D",
      "F",
      "G",
    ]);
  });

  test("finds shortest path between B and E", () => {
    expect(findShortestPath(graph0, "B", "E")).toEqual(["B", "C", "E"]);
  });

  test("returns null if no path is found", () => {
    expect(findShortestPath(graph0, "A", "H")).toBeNull();
  });
});

describe("findCycleInGraph", () => {
  // Test dla grafu zawierającego prosty cykl
  test("finds simple cycle in graph", () => {
    const graphWithSimpleCycle = {
      A: ["B"],
      B: ["C"],
      C: ["A"],
    };
    expect(findCycleInGraph(graphWithSimpleCycle)).toEqual([
      "A",
      "B",
      "C",
      "A",
    ]);
  });

  // Test dla grafu bez cyklu
  test("returns null for graph without cycle", () => {
    const graphWithoutCycle = {
      A: ["B"],
      B: ["C"],
      C: ["D"],
      D: [],
    };
    expect(findCycleInGraph(graphWithoutCycle)).toBeNull();
  });

  // Test dla grafu zawierającego cykl z więcej niż trzema wierzchołkami
  test("finds cycle with more than three vertices in graph", () => {
    const graphWithCycle = {
      A: ["B"],
      B: ["C"],
      C: ["D"],
      D: ["E"],
      E: ["A"],
    };
    expect(findCycleInGraph(graphWithCycle)).toEqual([
      "A",
      "B",
      "C",
      "D",
      "E",
      "A",
    ]);
  });

  // Test dla grafu zawierającego cykl z jednym wierzchołkiem
  test("finds cycle with single vertex in graph", () => {
    const graphWithSingleVertexCycle = {
      A: ["A"],
    };
    expect(findCycleInGraph(graphWithSingleVertexCycle)).toEqual(["A", "A"]);
  });

  // Test dla grafu zawierającego wiele cykli
  test("finds one of the cycles in graph with multiple cycles", () => {
    const graphWithMultipleCycles = {
      A: ["B", "C"],
      B: ["C"],
      C: ["D"],
      D: ["A", "E"],
      E: ["D"],
    };
    const expectedCycles = [
      ["A", "B", "C", "D", "A"],
      ["D", "E", "D"],
    ];
    expect(expectedCycles).toContainEqual(
      findCycleInGraph(graphWithMultipleCycles)
    );
  });
});

describe("sumPairExists", () => {
  test("Returns true if there exists a pair of numbers in the array that sum up to the target", () => {
    expect(sumPairExists([2, 7, 11, 15], 9)).toBe(true); // 2 + 7 = 9
    expect(sumPairExists([1, 2, 3, 4, 5], 6)).toBe(true); // 1 + 5 = 6
  });

  test("Returns false if there does not exist a pair of numbers in the array that sum up to the target", () => {
    expect(sumPairExists([2, 7, 11, 15], 10)).toBe(false);
    expect(sumPairExists([1, 2, 3, 4, 5], 2)).toBe(false);
  });

  test("Handles empty array", () => {
    expect(sumPairExists([], 10)).toBe(false);
  });

  test("Handles single element array", () => {
    expect(sumPairExists([5], 5)).toBe(false);
  });

  test("Handles negative numbers", () => {
    expect(sumPairExists([-1, -2, -3, -4], -5)).toBe(true); // -1 + -4 = -5
  });
});

describe("maxDifference", () => {
  test("Test for an empty array", () => {
    expect(maxDifference([])).toBe(0);
  });

  test("Test for an array with one element", () => {
    expect(maxDifference([5])).toBe(0);
  });

  test("Test for an array with increasing numbers", () => {
    expect(maxDifference([2, 3, 10, 2, 4, 8, 1])).toBe(8);
  });

  test("Test for an array with decreasing numbers", () => {
    expect(maxDifference([7, 9, 5, 6, 3, 2])).toBe(2);
  });

  test("Test for an array with numbers decreasing in reverse", () => {
    expect(maxDifference([10, 8, 6, 4, 2, 0])).toBe(0);
  });

  test("Test for an array with repeated numbers", () => {
    expect(maxDifference([5, 5, 5, 5, 5])).toBe(0);
  });
});

describe("sortByDigitSum", () => {
  test("Sorts array by digit sum", () => {
    expect(sortByDigitSum([123, 44, 51, 555, 99])).toEqual([
      123, 51, 44, 555, 99,
    ]);
  });

  test("Handles negative numbers", () => {
    expect(sortByDigitSum([-123, -44, -51, -555, -99])).toEqual([
      -123, -51, -44, -555, -99,
    ]);
  });

  test("Handles zeros", () => {
    expect(sortByDigitSum([0, 10, 100])).toEqual([0, 10, 100]);
  });

  test("Handles array with one element", () => {
    expect(sortByDigitSum([123])).toEqual([123]);
  });

  test("Handles empty array", () => {
    expect(sortByDigitSum([])).toEqual([]);
  });
});

describe("numberOfUniqueness", () => {
  test("Number of uniqueness for array with unique elements", () => {
    expect(numberOfUniqueness([1, 2, 3, 4, 5])).toBe(5);
  });

  test("Number of uniqueness for array with some duplicate elements", () => {
    expect(numberOfUniqueness([1, 2, 2, 3, 3, 3])).toBe(3);
  });

  test("Number of uniqueness for empty array", () => {
    expect(numberOfUniqueness([])).toBe(0);
  });

  test("Number of uniqueness for array with all duplicate elements", () => {
    expect(numberOfUniqueness([1, 1, 1, 1])).toBe(1);
  });
});

describe("maxProfit", () => {
  it("powinna zwrócić 5 dla [7, 1, 5, 3, 6, 4]", () => {
    expect(maxProfit([7, 1, 5, 3, 6, 4])).toEqual(5);
  });

  it("powinna zwrócić 0 dla [7, 6, 4, 3, 1]", () => {
    expect(maxProfit([7, 6, 4, 3, 1])).toEqual(0);
  });

  it("powinna zwrócić 2 dla [2, 4, 1]", () => {
    expect(maxProfit([2, 4, 1])).toEqual(2);
  });

  it("powinna zwrócić 0 dla []", () => {
    expect(maxProfit([])).toEqual(0);
  });
});

describe("findPairsWithDifference", () => {
  it("powinna zwrócić [[1, 3], [3, 5]] dla [1, 3, 5, 7] i k = 2", () => {
    expect(findPairsWithDifference([1, 3, 5, 7], 2)).toEqual([
      [3, 1],
      [5, 3],
      [7, 5],
    ]);
  });

  it("powinna zwrócić [[3, 1], [3, 5]] dla [1, 3, 5, 7] i k = -2", () => {
    expect(findPairsWithDifference([1, 3, 5, 7], -2)).toEqual([
      [1, 3],
      [3, 5],
      [5, 7],
    ]);
  });

  it("powinna zwrócić [[1, 5], [3, 7]] dla [1, 3, 5, 7] i k = 4", () => {
    expect(findPairsWithDifference([1, 3, 5, 7], 4)).toEqual([
      [5, 1],
      [7, 3],
    ]);
  });

  it("powinna zwrócić [] dla [1, 2, 3, 4] i k = 5", () => {
    expect(findPairsWithDifference([1, 2, 3, 4], 5)).toEqual([]);
  });
});

describe("calculateSumExceptSelf", () => {
  it("powinna zwrócić [6, 5, 4, 3, 2, 1] dla [1, 2, 3, 4, 5, 6]", () => {
    expect(calculateSumExceptSelf([1, 2, 3, 4, 5, 6])).toEqual([
      20, 19, 18, 17, 16, 15,
    ]);
  });

  it("powinna zwrócić [0, 0, 0, 0] dla [1, 1, 1, 1]", () => {
    expect(calculateSumExceptSelf([1, 1, 1, 1])).toEqual([3, 3, 3, 3]);
  });

  it("powinna zwrócić [8, 7, 6, 5, 4] dla [1, 2, 3, 4, 5]", () => {
    expect(calculateSumExceptSelf([1, 2, 3, 4, 5])).toEqual([
      14, 13, 12, 11, 10,
    ]);
  });

  it("powinna zwrócić [0] dla [0]", () => {
    expect(calculateSumExceptSelf([0])).toEqual([0]);
  });
});

describe("findEqualPairs", () => {
  it("powinna zwrócić poprawne pary dla tablicy zawierającej takie same liczby", () => {
    const inputArray = [1, 2, 3, 2, 4, 3, 5, 5];
    const expectedResult = [
      [1, 3],
      [2, 5],
      [6, 7],
    ];
    expect(findEqualPairs(inputArray)).toEqual(expectedResult);
  });

  it("powinna zwrócić puste tablice dla tablicy zawierającej różne liczby", () => {
    const inputArray = [1, 2, 3, 4, 5];
    expect(findEqualPairs(inputArray)).toEqual([]);
  });

  it("powinna zwrócić puste tablice dla pustej tablicy wejściowej", () => {
    const inputArray = [];
    expect(findEqualPairs(inputArray)).toEqual([]);
  });

  it("powinna zwrócić poprawne pary dla tablicy zawierającej takie same liczby na różnych pozycjach", () => {
    const inputArray = [3, 1, 4, 2, 3, 1, 4, 5];
    const expectedResult = [
      [0, 4],
      [1, 5],
      [2, 6],
    ];
    expect(findEqualPairs(inputArray)).toEqual(expectedResult);
  });
});

describe("parsingDom", () => {
  it("Dla poprawnie zbudowanego drzewa DOM zwraca true", () => {
    expect(parsingDom("<div><p>Hello world</p><a></a></div>")).toEqual(true);
  });
  it("Dla niepoprawnie zbudowanego drzewa DOM zwraca pierwszy element bez pary", () => {
    expect(parsingDom("<div><p>Hello world<a></a></div>")).toEqual("p");
  });
});

describe("Topological Sort", () => {
  it("Sortowanie topologiczne dla prostego grafu", () => {
    const graph = {
      A: ["B", "C"],
      B: ["D"],
      C: ["D", "E"],
      D: ["E"],
      E: [],
    };
    const expectedOutput = ["A", "B", "C", "D", "E"];
    expect(topologicalSort(graph)).toEqual(expectedOutput);
  });

  it("Sortowanie topologiczne dla grafu z pojedynczym wierzchołkiem", () => {
    const graph = {
      A: [],
    };
    const expectedOutput = ["A"];
    expect(topologicalSort(graph)).toEqual(expectedOutput);
  });

  it("Sortowanie topologiczne dla pustego grafu", () => {
    const graph = {};
    const expectedOutput = [];
    expect(topologicalSort(graph)).toEqual(expectedOutput);
  });
});

describe("longestSubstringWithoutRepeating", () => {
  test("should return 0 for an empty string", () => {
    expect(longestSubstringWithoutRepeating("")).toBe(0);
  });

  test("should return the length of the string for a string with unique characters", () => {
    expect(longestSubstringWithoutRepeating("abc")).toBe(3);
    expect(longestSubstringWithoutRepeating("abcdef")).toBe(6);
  });

  test("should 0return the length of the longest substring without repeating characters", () => {
    expect(longestSubstringWithoutRepeating("abcabcbb")).toBe(3); // 'abc'
    expect(longestSubstringWithoutRepeating("bbbbb")).toBe(1); // 'b'
    expect(longestSubstringWithoutRepeating("pwwkew")).toBe(3); // 'wke'
    expect(longestSubstringWithoutRepeating("aab")).toBe(2); // 'ab'
    expect(longestSubstringWithoutRepeating("dvdf")).toBe(3); // 'vdf'
  });

  test("should handle strings with all repeating characters", () => {
    expect(longestSubstringWithoutRepeating("aaaaaa")).toBe(1); // 'a'
    expect(longestSubstringWithoutRepeating("bbbb")).toBe(1); // 'b'
    expect(longestSubstringWithoutRepeating("cccccccccccc")).toBe(1); // 'c'
  });

  test("should handle strings with one character", () => {
    expect(longestSubstringWithoutRepeating("a")).toBe(1); // 'a'
  });
});

describe("decimalToBinary", () => {
  test('should return "0" for input 0', () => {
    expect(decimalToBinary(0)).toBe("0");
  });

  test('should return "101" for input 5', () => {
    expect(decimalToBinary(5)).toBe("101");
  });

  test('should return "1010" for input 10', () => {
    expect(decimalToBinary(10)).toBe("1010");
  });

  test('should return "1111" for input 15', () => {
    expect(decimalToBinary(15)).toBe("1111");
  });

  test('should return "10001" for input 17', () => {
    expect(decimalToBinary(17)).toBe("10001");
  });

  test('should return "11111111" for input 255', () => {
    expect(decimalToBinary(255)).toBe("11111111");
  });
});

describe("maxSubarraySumLessThanTarget", () => {
  test("should return [2, 3, 4] for input [1, 2, 3, 4, 5] and target 9", () => {
    expect(maxSubarraySumLessThanTarget([1, 2, 3, 4, 5], 9)).toEqual([1, 2, 3]);
  });

  test("should return [1, 2, 3, 4, 5] for input [1, 2, 3, 4, 5] and target 11", () => {
    expect(maxSubarraySumLessThanTarget([1, 2, 3, 4, 5], 11)).toEqual([
      1, 2, 3, 4,
    ]);
  });

  test("should return [1, 2] for input [1, 2, 3, 4, 5] and target 5", () => {
    expect(maxSubarraySumLessThanTarget([1, 2, 3, 4, 5], 5)).toEqual([1, 2]);
  });

  test("should return [] for input [1, 2, 3, 4, 5] and target 0", () => {
    expect(maxSubarraySumLessThanTarget([1, 2, 3, 4, 5], 0)).toEqual([]);
  });

  test("should return [] for input [] and target 5", () => {
    expect(maxSubarraySumLessThanTarget([], 5)).toEqual([]);
  });
});

describe("longestCommonSubsequenceLength", () => {
  test("empty strings", () => {
    expect(longestCommonSubsequenceLength("", "")).toBe(0);
  });

  test("one empty string", () => {
    expect(longestCommonSubsequenceLength("abc", "")).toBe(0);
    expect(longestCommonSubsequenceLength("", "xyz")).toBe(0);
  });

  test("no common subsequence", () => {
    expect(longestCommonSubsequenceLength("abc", "xyz")).toBe(0);
    expect(longestCommonSubsequenceLength("1234", "5678")).toBe(0);
  });

  test("one common subsequence", () => {
    expect(longestCommonSubsequenceLength("abcd", "a123b4c5d6")).toBe(4);
    expect(longestCommonSubsequenceLength("abcdef", "1a2b3c4d5e6f")).toBe(6);
  });

  test("identical strings", () => {
    expect(longestCommonSubsequenceLength("abcd", "abcd")).toBe(4);
  });
});

describe("longestCommonSubsequence", () => {
  test("empty strings", () => {
    expect(longestCommonSubsequence("", "")).toBe("");
  });

  test("no common subsequence", () => {
    expect(longestCommonSubsequence("abc", "xyz")).toBe("");
  });

  test("one common subsequence", () => {
    expect(longestCommonSubsequence("abcd", "a123b4c5d6")).toBe("abcd");
    expect(longestCommonSubsequence("abcdef", "1a2b3c4d5e6f")).toBe("abcdef");
  });

  test("identical strings", () => {
    expect(longestCommonSubsequence("abcd", "abcd")).toBe("abcd");
  });
});

describe("findLongestIncreasingSubsequenceLength", () => {
  test("returns 0 for an empty array", () => {
    expect(findLongestIncreasingSubsequenceLength([])).toBe(0);
  });

  test("returns 1 for an array containing a single element", () => {
    expect(findLongestIncreasingSubsequenceLength([5])).toBe(1);
  });

  test("returns correct length for an unsorted array", () => {
    expect(
      findLongestIncreasingSubsequenceLength([1, 3, 5, 4, 7, 8, 2, 10])
    ).toBe(6);
  });

  test("returns correct length for an array sorted in ascending order", () => {
    expect(findLongestIncreasingSubsequenceLength([1, 2, 3, 4, 5])).toBe(5);
  });

  test("returns correct length for an array sorted in descending order", () => {
    expect(findLongestIncreasingSubsequenceLength([5, 4, 3, 2, 1])).toBe(1);
  });

  test("returns correct length for an array containing duplicates", () => {
    expect(findLongestIncreasingSubsequenceLength([1, 2, 3, 1, 2, 3])).toBe(3);
  });
});

describe("findLongestIncreasingSubsequence2", () => {
  test("returns an empty array for an empty input array", () => {
    expect(findLongestIncreasingSubsequence2([])).toEqual([]);
  });

  test("returns correct subsequence for an unsorted array", () => {
    expect(
      findLongestIncreasingSubsequence2([1, 3, 5, 4, 7, 8, 2, 10])
    ).toEqual([1, 3, 5, 7, 8, 10]);
  });

  test("returns correct subsequence for an array sorted in ascending order", () => {
    expect(findLongestIncreasingSubsequence2([1, 2, 3, 4, 5])).toEqual([
      1, 2, 3, 4, 5,
    ]);
  });

  test("returns correct subsequence for an array sorted in descending order", () => {
    expect(findLongestIncreasingSubsequence2([5, 4, 3, 2, 1])).toEqual([5]);
  });

  test("returns correct subsequence for an array containing duplicates", () => {
    expect(findLongestIncreasingSubsequence2([1, 2, 3, 1, 2, 3])).toEqual([
      1, 2, 3,
    ]);
    expect(findLongestIncreasingSubsequence2([2, 4, 2, 6, 1, 4, 8])).toEqual([
      2, 4, 6, 8,
    ]);
  });
});

describe("Greatest Common Divisor (GCD)", () => {
  test("GCD of two prime numbers", () => {
    expect(gcd(3, 5)).toBe(1);
  });

  test("GCD of one with another number", () => {
    expect(gcd(1, 7)).toBe(1);
  });

  test("GCD of two equal numbers", () => {
    expect(gcd(10, 10)).toBe(10);
  });

  test("GCD of two even numbers", () => {
    expect(gcd(12, 18)).toBe(6);
  });

  test("GCD of two odd numbers", () => {
    expect(gcd(15, 35)).toBe(5);
  });

  test("GCD of two numbers of different sizes", () => {
    expect(gcd(36, 9)).toBe(9);
  });

  test("GCD of two large numbers", () => {
    expect(gcd(123456, 7890)).toBe(6);
  });

  test("GCD of two zeros", () => {
    expect(gcd(0, 0)).toBe(0);
  });

  test("GCD of zero with another number", () => {
    expect(gcd(0, 15)).toBe(15);
  });
});

describe("sumOfDigits", () => {
  test("Sum of digits for a single digit number", () => {
    expect(sumOfDigits(5)).toBe(5);
  });

  test("Sum of digits for a multiple digit number", () => {
    expect(sumOfDigits(12345)).toBe(15);
  });

  test("Sum of digits for a zero", () => {
    expect(sumOfDigits(0)).toBe(0);
  });

  test("Sum of digits for a large number", () => {
    expect(sumOfDigits(987654321)).toBe(45);
  });

  test("Sum of digits for a negative number", () => {
    expect(sumOfDigits(-123)).toBe(6); // Suma cyfr z liczby bez minusa
  });

  test("Sum of digits for a negative number with multiple digits", () => {
    expect(sumOfDigits(-4567)).toBe(22); // Suma cyfr z liczby bez minusa
  });
});

describe("isPerfectNumber", () => {
  it("should return true for perfect numbers", () => {
    expect(isPerfectNumber(6)).toBe(true);
    expect(isPerfectNumber(28)).toBe(true);
    expect(isPerfectNumber(496)).toBe(true);
    expect(isPerfectNumber(8128)).toBe(true);
  });

  it("should return false for non-perfect numbers", () => {
    expect(isPerfectNumber(0)).toBe(false);
    expect(isPerfectNumber(12)).toBe(false);
    expect(isPerfectNumber(30)).toBe(false);
    expect(isPerfectNumber(100)).toBe(false);
  });
});

describe("perfectNumbersLowerThan", () => {
  it("should return all perfect numbers in the specified range", () => {
    expect(perfectNumbersLowerThan(10000)).toEqual([6, 28, 496, 8128]);
  });

  it("should return an empty array if no perfect numbers found", () => {
    expect(perfectNumbersLowerThan(10)).toEqual([6]);
  });

  it("should handle upper limit of 1", () => {
    expect(perfectNumbersLowerThan(1)).toEqual([]);
  });
});
describe("longestPalindrome", () => {
  it("should return the longest palindrome in the given string", () => {
    expect(longestPalindrome("babad")).toBe("bab");
    expect(longestPalindrome("cbbd")).toBe("bb");
    expect(longestPalindrome("a")).toBe("a");
    expect(longestPalindrome("ac")).toBe("a");
  });

  it("should handle empty string", () => {
    expect(longestPalindrome("")).toBe("");
  });

  it("should handle string with all the same characters", () => {
    expect(longestPalindrome("aaa")).toBe("aaa");
    expect(longestPalindrome("bbbbbb")).toBe("bbbbbb");
  });
});

describe("longestVowelSubstring", () => {
  test('Długość najdłuższego ciągu spójnych samogłosek w "education" powinna być 2', () => {
    expect(longestVowelSubstring("education")).toBe(2);
  });

  test('Długość najdłuższego ciągu spójnych samogłosek w "aieee" powinna być 5', () => {
    expect(longestVowelSubstring("aieee")).toBe(5);
  });

  test('Długość najdłuższego ciągu spójnych samogłosek w "bcd" powinna być 0', () => {
    expect(longestVowelSubstring("bcd")).toBe(0);
  });

  test('Długość najdłuższego ciągu spójnych samogłosek w "aeiou" powinna być 5', () => {
    expect(longestVowelSubstring("aeiou")).toBe(5);
  });

  test('Długość najdłuższego ciągu spójnych samogłosek w "x" powinna być 0', () => {
    expect(longestVowelSubstring("x")).toBe(0);
  });
});

describe("countUniqueElements", () => {
  it("should return an array of unique elements", () => {
    expect(countUniqueElements([1, 2, 3, 2, 4, 5, 1, 6, 3])).toEqual([4, 5, 6]);
  });

  it("should return an empty array if all elements are duplicates", () => {
    expect(countUniqueElements([1, 1, 1, 1, 1])).toEqual([]);
  });

  it("should return the same array if there are no duplicates", () => {
    expect(countUniqueElements([1, 2, 3, 4, 5])).toEqual([1, 2, 3, 4, 5]);
  });

  it("should handle an empty array", () => {
    expect(countUniqueElements([])).toEqual([]);
  });
});

describe("Counting Sort", () => {
  test("Sortowanie listy", () => {
    const input = [4, 2, 2, 8, 3, 3, 1];
    const expectedOutput = [1, 2, 2, 3, 3, 4, 8];
    expect(countingSort(input)).toEqual(expectedOutput);
  });

  test("Sortowanie pustej listy", () => {
    const input = [];
    const expectedOutput = [];
    expect(countingSort(input)).toEqual(expectedOutput);
  });
});

describe("longestCommonSubsequence", () => {
  it("Powinien zwrócić najdłuższy wspólny podciąg", () => {
    // Testujemy dla dwóch ciągów znaków o różnej długości
    expect(longestCommonSubsequence("abcde", "ace")).toBe("ace");

    expect(longestCommonSubsequence("a1b2c34de", "5a6ce")).toBe("ace");

    // Testujemy dla dwóch identycznych ciągów
    expect(longestCommonSubsequence("abcdef", "abcdef")).toBe("abcdef");

    // Testujemy dla dwóch całkowicie różnych ciągów
    expect(longestCommonSubsequence("abc", "def")).toBe("");

    // Testujemy dla pustych ciągów
    expect(longestCommonSubsequence("", "")).toBe("");

    // Testujemy dla jednego pustego ciągu
    expect(longestCommonSubsequence("abc", "")).toBe("");
    expect(longestCommonSubsequence("", "def")).toBe("");
  });
});

describe("maxSubarraySum", () => {
  it("Powinno zwrócić poprawną największą sumę podciągu", () => {
    // Test dla tablicy zawierającej dodatnie liczby
    expect(maxSubarraySum([1, 2, 3])).toBe(6);

    // Test dla tablicy zawierającej zarówno dodatnie, jak i ujemne liczby
    expect(maxSubarraySum([1, -2, 3, -1, 2])).toBe(4);

    // Test dla pustej tablicy
    expect(maxSubarraySum([])).toBe(0);

    // Test dla tablicy zawierającej tylko jedną liczbę
    expect(maxSubarraySum([-1])).toBe(-1);

    // Test dla tablicy zawierającej wszystkie ujemne liczby
    expect(maxSubarraySum([-1, -2, -3])).toBe(-1);
  });
});

test("countWords should count occurrences of words correctly", () => {
  expect(countWords("hello world hello")).toEqual({ hello: 2, world: 1 });
  expect(countWords("the quick brown fox jumps over the lazy dog")).toEqual({
    the: 2,
    quick: 1,
    brown: 1,
    fox: 1,
    jumps: 1,
    over: 1,
    lazy: 1,
    dog: 1,
  });
});

test("countWords should handle empty string", () => {
  expect(countWords("")).toEqual({});
});

test("countWords should handle string with only one word", () => {
  expect(countWords("hello")).toEqual({ hello: 1 });
});

test("countWords should handle string with multiple spaces between words", () => {
  expect(countWords("hello    world")).toEqual({ hello: 1, world: 1 });
});

test("countWords should handle string with mixed case words", () => {
  expect(countWords("Hello World hello")).toEqual({ hello: 2, world: 1 });
});

describe("longestZeroSumSubarray", () => {
  test("returns the longest zero sum subarray for a given array", () => {
    expect(longestZeroSumSubarray([4, 2, -3, 1, 6])).toEqual([2, -3, 1]);
    expect(longestZeroSumSubarray([2, -5, 1, 2, -1, 3, 4, -2, -2])).toEqual([
      -5, 1, 2, -1, 3, 4, -2, -2,
    ]);
  });

  test("returns an empty array if there is no zero sum subarray", () => {
    expect(longestZeroSumSubarray([1, 2, 3, 4, 5])).toEqual([]);
    expect(longestZeroSumSubarray([-1, -2, -3, -4, -5])).toEqual([]);
  });

  test("handles arrays with only one element", () => {
    expect(longestZeroSumSubarray([0])).toEqual([0]);
    expect(longestZeroSumSubarray([5])).toEqual([]);
  });

  test("handles arrays with all zero elements", () => {
    expect(longestZeroSumSubarray([0, 0, 0, 0])).toEqual([0, 0, 0, 0]);
  });

  test("handles arrays with negative and positive elements", () => {
    expect(longestZeroSumSubarray([2, -1, 1, -1, 1])).toEqual([-1, 1, -1, 1]);
    expect(longestZeroSumSubarray([-2, 1, 1, -1, 2])).toEqual([-2, 1, 1]);
  });
});

test("Empty string should return 0", () => {
  expect(longestSubstringWithoutRepeatingCharacters("")).toBe(0);
});

test('String "abcabcbb" should return 3', () => {
  expect(longestSubstringWithoutRepeatingCharacters("abcabcbb")).toBe(3);
});

test('String "bbbbb" should return 1', () => {
  expect(longestSubstringWithoutRepeatingCharacters("bbbbb")).toBe(1);
});

test('String "pwwkew" should return 4', () => {
  expect(longestSubstringWithoutRepeatingCharacters("pwwkew")).toBe(4);
});

test('String "abcdefg" should return 7', () => {
  expect(longestSubstringWithoutRepeatingCharacters("abcdefg")).toBe(7);
});

describe("treeConstructor", () => {
  test("Returns true for valid tree structure", () => {
    expect(treeConstructor(["(1,2)", "(2,4)", "(5,7)", "(7,2)", "(9,5)"])).toBe(
      true
    );
    expect(treeConstructor(["(1,2)", "(2,4)", "(3,4)"])).toBe(true);
  });

  test("Returns false for invalid tree structure", () => {
    expect(treeConstructor(["(1,2)", "(3,4)"])).toBe(false);
  });
});

test("lengthOfLongestSubstring finds longest substring without repeating characters", () => {
  expect(lengthOfLongestSubstring("abcabcbb")).toBe(3); // "abc"
  expect(lengthOfLongestSubstring("bbbbb")).toBe(1); // "b"
  expect(lengthOfLongestSubstring("pwwkew")).toBe(3); // "wke"
  expect(lengthOfLongestSubstring("")).toBe(0); // ""
  expect(lengthOfLongestSubstring("abcdefg")).toBe(7); // "abcdefg"
  expect(lengthOfLongestSubstring("aab")).toBe(2); // "ab"
  expect(lengthOfLongestSubstring("dvdf")).toBe(3); // "vdf"
});

test("lengthOfLongestSubstring handles single character strings", () => {
  expect(lengthOfLongestSubstring("a")).toBe(1);
  expect(lengthOfLongestSubstring(" ")).toBe(1);
});

test("lengthOfLongestSubstring handles strings with all unique characters", () => {
  expect(lengthOfLongestSubstring("abcdefghijklmnopqrstuvwxyz")).toBe(26);
});

test("Znajduje maksymalny podciąg w tablicy z wartościami dodatnimi i ujemnymi", () => {
  const result = maxSubarray([-2, 1, -3, 4, -1, 2, 1, -5, 4]);
  expect(result).toEqual([4, -1, 2, 1]);
});

test("Radzi sobie z tablicą składającą się tylko z jednego elementu", () => {
  const result = maxSubarray([1]);
  expect(result).toEqual([1]);
});

test("Radzi sobie z tablicą składającą się z samych wartości ujemnych", () => {
  const result = maxSubarray([-1, -2, -3, -4]);
  expect(result).toEqual([-1]);
});

test("Radzi sobie z pustą tablicą", () => {
  const result = maxSubarray([]);
  expect(result).toEqual([]);
});

test("Radzi sobie z tablicą, w której maksymalny podciąg to cała tablica", () => {
  const result = maxSubarray([1, 2, 3, 4]);
  expect(result).toEqual([1, 2, 3, 4]);
});

test("Radzi sobie z tablicą z mieszanymi wartościami dodatnimi i ujemnymi", () => {
  const result = maxSubarray([-1, 2, 3, -5, 4, -1, 2, 1, -5, 4]);
  expect(result).toEqual([4, -1, 2, 1]);
});

test("Znajduje brakującą liczbę w krótkiej tablicy", () => {
  expect(missingNumber([3, 0, 1])).toBe(2);
});

test("Znajduje brakującą liczbę w dłuższej tablicy", () => {
  expect(missingNumber([9, 6, 4, 2, 3, 5, 7, 0, 1])).toBe(8);
});

test("Radzi sobie z pustą tablicą", () => {
  expect(missingNumber([])).toBe(0);
});

test("Radzi sobie z tablicą zawierającą tylko jeden element", () => {
  expect(missingNumber([0])).toBe(1);
  expect(missingNumber([1])).toBe(0);
});

test("Radzi sobie z tablicą bez brakujących liczb (zakres 0-1)", () => {
  expect(missingNumber([0, 1])).toBe(2);
});

test("Longest Increasing Subsequence of [10, 9, 2, 5, 3, 7, 101, 18] is 4", () => {
  const nums = [10, 9, 2, 5, 3, 7, 101, 18];
  expect(longestIncreasingSubsequence(nums)).toBe(4);
});

test("Longest Increasing Subsequence of [0, 1, 0, 3, 2, 3] is 4", () => {
  const nums = [0, 1, 0, 3, 2, 3];
  expect(longestIncreasingSubsequence(nums)).toBe(4);
});

test("Longest Increasing Subsequence of [7, 7, 7, 7, 7, 7, 7] is 1", () => {
  const nums = [7, 7, 7, 7, 7, 7, 7];
  expect(longestIncreasingSubsequence(nums)).toBe(1);
});

test("Longest Increasing Subsequence of [3, 10, 2, 1, 20] is 3", () => {
  const nums = [3, 10, 2, 1, 20];
  expect(longestIncreasingSubsequence(nums)).toBe(3);
});

test("Longest Increasing Subsequence of an empty array is 0", () => {
  const nums = [];
  expect(longestIncreasingSubsequence(nums)).toBe(0);
});

test("Longest Increasing Subsequence of [10, 22, 9, 33, 21, 50, 41, 60, 80] is 6", () => {
  const nums = [10, 22, 9, 33, 21, 50, 41, 60, 80];
  expect(longestIncreasingSubsequence(nums)).toBe(6);
});

test("isHappyNumber function correctly identifies happy numbers", () => {
  expect(isHappyNumber(19)).toBe(true); // 19 is a happy number
  expect(isHappyNumber(1)).toBe(true); // 1 is a happy number
  expect(isHappyNumber(7)).toBe(true); // 7 is a happy number
});

test("isHappyNumber function correctly identifies non-happy numbers", () => {
  expect(isHappyNumber(2)).toBe(false); // 2 is not a happy number
  expect(isHappyNumber(3)).toBe(false); // 3 is not a happy number
  expect(isHappyNumber(4)).toBe(false); // 4 is not a happy number
});

test("isHappyNumber function handles edge cases", () => {
  expect(isHappyNumber(0)).toBe(false); // 0 is not a happy number
  expect(isHappyNumber(-1)).toBe(false); // Negative numbers are not happy
  expect(isHappyNumber(1000000)).toBe(true); // Very large number, testing performance
});

describe("longestPalindromicSubstring", () => {
  test('znajduje najdłuższy palindrom w "babad"', () => {
    const result = longestPalindromicSubstring("babad");
    expect(["bab", "aba"]).toContain(result); // Mogą być dwa poprawne wyniki
  });

  test('znajduje najdłuższy palindrom w "cbbd"', () => {
    const result = longestPalindromicSubstring("cbbd");
    expect(result).toBe("bb");
  });

  test('znajduje najdłuższy palindrom w "a"', () => {
    const result = longestPalindromicSubstring("a");
    expect(result).toBe("a");
  });

  test('znajduje najdłuższy palindrom w "ac"', () => {
    const result = longestPalindromicSubstring("ac");
    expect(["a", "c"]).toContain(result);
  });

  test("znajduje najdłuższy palindrom w pustym ciągu", () => {
    const result = longestPalindromicSubstring("");
    expect(result).toBe("");
  });

  test("znajduje najdłuższy palindrom w długim ciągu", () => {
    const result = longestPalindromicSubstring("forgeeksskeegfor");
    expect(result).toBe("geeksskeeg");
  });
});

describe("firstUniqChar", () => {
  test('znajduje pierwszy niepowtarzający się znak w "leetcode"', () => {
    const result = firstUniqChar("leetcode");
    expect(result).toBe(0);
  });

  test('znajduje pierwszy niepowtarzający się znak w "loveleetcode"', () => {
    const result = firstUniqChar("loveleetcode");
    expect(result).toBe(2);
  });

  test('sprawdza, że nie ma niepowtarzających się znaków w "aabb"', () => {
    const result = firstUniqChar("aabb");
    expect(result).toBe(-1);
  });

  test('znajduje pierwszy niepowtarzający się znak w "aabbccd"', () => {
    const result = firstUniqChar("aabbccd");
    expect(result).toBe(6);
  });

  test("sprawdza pusty napis", () => {
    const result = firstUniqChar("");
    expect(result).toBe(-1);
  });

  test('znajduje pierwszy niepowtarzający się znak w "abcdef"', () => {
    const result = firstUniqChar("abcdef");
    expect(result).toBe(0);
  });

  test('znajduje pierwszy niepowtarzający się znak w "aaabcccdeeef"', () => {
    const result = firstUniqChar("aaabcccdeeef");
    expect(result).toBe(3);
  });
});

test('znajduje najkrótszą ścieżkę w prostym labiryncie', () => {
  const maze = [
    [0, 1, 0, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 1, 0],
    [1, 1, 0, 1, 0],
    [0, 0, 0, 0, 0]
  ];
  const expected = [[0, 0], [1, 0], [2, 0], [2, 1], [2, 2], [3, 2], [4, 2], [4, 3], [4, 4]];
  expect(findShortestPathMaze(maze)).toEqual(expected);
});

test('zwraca null, gdy nie ma ścieżki', () => {
  const maze = [
    [0, 1, 0],
    [1, 1, 0],
    [0, 0, 1]
  ];
  expect(findShortestPathMaze(maze)).toBeNull();
});

test('działa dla labiryntu z jednym rzędem', () => {
  const maze = [
    [0, 0, 0, 0, 0]
  ];
  const expected = [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4]];
  expect(findShortestPathMaze(maze)).toEqual(expected);
});

test('działa dla labiryntu z jedną kolumną', () => {
  const maze = [
    [0],
    [0],
    [0],
    [0],
    [0]
  ];
  const expected = [[0, 0], [1, 0], [2, 0], [3, 0], [4, 0]];
  expect(findShortestPathMaze(maze)).toEqual(expected);
});

test('działa dla labiryntu o skomplikowanym układzie', () => {
  const maze = [
    [0, 0, 1, 0],
    [1, 0, 1, 0],
    [1, 0, 0, 0],
    [1, 1, 1, 0]
  ];
  const expected = [[0, 0], [0, 1], [1, 1], [2, 1], [2, 2], [2, 3], [3, 3]];
  expect(findShortestPathMaze(maze)).toEqual(expected);
});
