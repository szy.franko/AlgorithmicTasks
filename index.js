function findDuplicates(array) {
  const duplicatesObj = {};
  for (let i = 0; i < array.length; i++) {
    if (duplicatesObj[array[i]]) {
      duplicatesObj[array[i]]++;
    } else {
      duplicatesObj[array[i]] = 1;
    }
  }

  return Object.keys(duplicatesObj)
    .filter((el) => {
      return duplicatesObj[el] > 1;
    })
    .map((el) => parseInt(el));
}

function flattenArray(array) {
  let output = [];
  for (let i = 0, max = array.length; i < max; i++) {
    if (Array.isArray(array[i])) {
      output = output.concat(flattenArray(array[i]));
    } else {
      output.push(array[i]);
    }
  }

  return output;
}

function isPalindrome(word) {
  return (
    word
      .split("")
      .filter((el) => {
        return el !== " " && el !== ",";
      })
      .map((el) => el.toLowerCase())
      .reverse()
      .join("") ==
    word
      .split("")
      .filter((el) => {
        return el !== " " && el !== ",";
      })
      .map((el) => el.toLowerCase())
      .join("")
  );
}

function duplicateCount(string) {
  const array = string.split("").map((el) => el.toLowerCase());
  const duplicatesObj = {};
  for (let i = 0; i < array.length; i++) {
    if (duplicatesObj[array[i]]) {
      duplicatesObj[array[i]]++;
    } else {
      duplicatesObj[array[i]] = 1;
    }
  }

  return Object.keys(duplicatesObj)
    .filter((el) => {
      return duplicatesObj[el] > 1;
    })
    .map((el) => parseInt(el)).length;
}

function reverseWords(word) {
  return word
    .split(" ")
    .map((el) => el.split("").reverse().join(""))
    .join(" ");
}

function mergeArrays(array0, array1) {
  const output = [];
  while (array0.length > 0 || array1.length > 0) {
    if (array0.length === 0) {
      return [...output, ...array1];
    }
    if (array1.length === 0) {
      return [...output, ...array0];
    }

    if (array0[0] < array1[0]) {
      output.push(array0.shift());
    } else if (array0[0] > array1[0]) {
      output.push(array1.shift());
    } else {
      output.push(array0.shift());
      output.push(array1.shift());
    }
  }
  return output;
}

function findMissingNumber(array) {
  const sortedArrray = array.sort((a, b) => a - b);
  for (let i = 1, max = sortedArrray.length; i < max; i++) {
    if (sortedArrray[i - 1] + 1 !== sortedArrray[i]) {
      return sortedArrray[i - 1] + 1;
    }
  }
}

function rotateArray(array, k) {
  let copy = [...array];

  let suffix = copy.slice(copy.length - k);
  copy.splice(copy.length - k);
  return suffix.concat(copy);
}

function findMinMax(array) {
  if (array.length === 0) {
    return "Array is empty";
  }
  const sorted = array.sort((a, b) => a - b);
  return { smallest: sorted[0], largest: sorted.at(-1) };
}

function firstNonRepeatingCharacter(word) {
  let array = word.split("");
  const founded = [];
  for (let i = 0; i < word.length; i++) {
    if (
      !array.slice(i + 1).find((el) => el === array[i]) &&
      founded.indexOf(array[i]) < 0
    ) {
      return word[i];
    } else {
      founded.push(word[i]);
    }
  }
  return null;
}

function sotrWord(word) {
  return word.split("").sort().join("");
}

function findAllAnagrams(array) {
  let output = [];
  let classes = new Map();
  let sortedWord;
  for (let i = 0, max = array.length; i < max; i++) {
    sortedWord = sotrWord(array[i]);
    if (!classes.get(sortedWord)) {
      classes.set(sortedWord, [array[i]]);
    } else {
      classes.get(sortedWord).push(array[i]);
    }
  }

  for (let [key, value] of classes) {
    output.push(value);
  }

  return output;
}

function fibonacci(n) {
  if (n === 0) return 0;
  if (n === 1 || n === 2) {
    return 1;
  }
  fibonacci.values = [0, 1, 1];
  if (fibonacci.values[n]) return fibonacci.values[n];
  return fibonacci(n - 1) + fibonacci(n - 2);
}

function noRepetition(s) {
  return new Set(s.split("")).size === s.length;
}
function longestSubstring(s) {
  if (s === "") return "";
  let longest = s[0];
  let substring;
  for (let i = 0, max = s.length; i < max; i++) {
    for (let j = i + 1, max0 = s.length; j < max0; j++) {
      substring = s.substring(i, j + 1);
      if (noRepetition(substring)) {
        if (substring.length > longest.length) {
          longest = substring;
        }
      }
    }
  }
  return longest;
}

function areAnagrams(str1, str2) {

  const cleanStr1 = str1.replace(/[^a-z]/gi, "").toLowerCase();
  const cleanStr2 = str2.replace(/[^a-z]/gi, "").toLowerCase();
  if(cleanStr1.length!==cleanStr2.length) { return false; }
  const count={};
  for(let i=0, max=cleanStr1.length; i<max; i++) {
    if(!count[cleanStr1[i]]) {
      count[cleanStr1[i]]=0;
    }
    if(!count[cleanStr2[i]]) {
      count[cleanStr2[i]]=0;
    }

    count[cleanStr1[i]] = count[cleanStr1[i]]+1;
    count[cleanStr2[i]] = count[cleanStr2[i]]-1;

  }
  for(let char in count) {
    if(count[char]!==0) {
      return false;
    }
  }
  return true;

  // return (
  //   cleanStr1.split("").sort().join("") === cleanStr2.split("").sort().join("")
  // );
}

function findIntersection(arr1, arr2) {
  const set1 = new Set(arr1);
  return Array.from(new Set(arr2.filter((el) => set1.has(el))));
}

function findMostFrequent(arr) {
  if (arr.length === 0) return null;
  const frequencyMap = new Map();

  arr.forEach((element) => {
    frequencyMap.set(element, (frequencyMap.get(element) || 0) + 1);
  });
  let mostFrequent;
  let highestFrequency = 0;
  frequencyMap.forEach((frequency, element) => {
    if (frequency > highestFrequency) {
      highestFrequency = frequency;
      mostFrequent = element;
    }
  });
  return mostFrequent;
}

function isPrimeNumber(number) {
  const primes = [];
  for (let i = 0; i <= number; i++) {
    primes.push(true);
  }
  primes[0] = false;
  primes[1] = false;
  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (primes[i]) {
      for (let j = 2; i * j <= number; j++) {
        primes[i * j] = false;
      }
    }
  }
  return primes[number];
}

function reverseWordsInSentence(sentence) {
  const words = sentence
    .split(" ")
    .filter((el) => el !== "")
    .map((word) => word.replace(/[^A-Za-z\u0100-\u017F]/g, ""));
  let prefix;
  for (let i = 0, max = words.length; i < max; i++) {
    words[i] = words[i].split("").reverse().join("");
    prefix = words[i].match(/^[,;!]/);
    if (prefix) {
      words[i] = words[i].slice(1) + prefix;
    }
  }
  return words.join(" ");
}

function findMaxMinDifference(numbers) {
  numbers = numbers.sort((a, b) => a - b);
  return numbers.at(-1) - numbers[0];
}
function findPairsWithSum(arr, target) {
  const pairs = [];
  const checked = {};
  for (let i = 0, max = arr.length; i < max; i++) {
    let complement = target - arr[i];
    if (checked[complement]) {
      pairs.push([complement, arr[i]]);
    }
    checked[arr[i]] = true;
  }
  return pairs;
}

function isPermutationPalindrome(word) {
  const cleanWord = word.replace(/\s/g, "");
  const objectLetters = cleanWord.split("").reduce((acc, current) => {
    if (acc[current]) {
      acc[current]++;
    } else {
      acc[current] = 1;
    }
    return acc;
  }, {});

  const odd = Object.values(objectLetters).filter((number) => number % 2 !== 0);
  return odd.length <= 1;
}

function isPangram(word) {
  const cleanWord = word.replace(/[\s\.,:;?]/g, "");
  const setOfLetters = new Set(
    cleanWord.split("").map((letter) => letter.toLowerCase())
  );
  return setOfLetters.size >= 26;
}

function findLongestNonDecraisingSubsequence(array) {
  if (array.length === 0) {
    return [];
  }

  let sequences = [];
  let presequences;
  let added = false;

  for (let i = 0; i < array.length; i++) {
    presequences = [];
    for (let j = 0, max = sequences.length; j < max; j++) {
      if (sequences[j] && sequences[j].at(-1) <= array[i]) {
        presequences.push([...sequences[j]]);
        sequences[j].push(array[i]);

        added = true;
      }
    }
    if (!added) {
      sequences.push([array[i]]);
    }

    added = false;
    sequences = sequences.concat(presequences);
  }

  sequences = sequences.sort((a, b) => b.length - a.length);
  return sequences[0];
}

function findMaxProductOfTwo(array) {
  if (array.length < 2) return undefined;
  array = array.sort((a, b) => a - b);
  return Math.max(array[0] * array[1], array.at(-2) * array.at(-1));
}

function findLongestIncreasingSubsequence(array) {
  if (array.length === 0) {
    return [];
  }

  let sequences = [];
  let presequences;
  let added = false;

  for (let i = 0; i < array.length; i++) {
    presequences = [];
    for (let j = 0, max = sequences.length; j < max; j++) {
      if (sequences[j] && sequences[j].at(-1) < array[i]) {
        presequences.push([...sequences[j]]);
        sequences[j].push(array[i]);

        added = true;
      }
    }
    if (!added) {
      sequences.push([array[i]]);
    }

    added = false;
    sequences = sequences.concat(presequences);
  }

  sequences = sequences.sort((a, b) => b.length - a.length);
  return sequences[0];
}

function findPairsWithSum2(array, target) {
  const pairs = [];
  const visited = new Set();
  const foundPairs = {};
  let pair;
  let key;
  for (let num of array) {
    if (num * 2 === target) {
      key = [num, num].join(",");
      if (!foundPairs[key]) {
        pairs.push([num, num]);
        foundPairs[key] = true;
      }
    }
    if (visited.has(target - num)) {
      pair = [num, target - num].sort((a, b) => a - b);

      key = pair.join(",");
      if (!foundPairs[key]) {
        pairs.push(pair);
        foundPairs[key] = true;
      }
    }
    visited.add(num);
  }
  return pairs;
}

function findMissingNumbersInRange(array, lower, upper) {
  array = array.sort((a, b) => a - b);
  const output = [];
  for (let i = lower; i <= upper; i++) {
    if (!array.includes(i)) {
      output.push(i);
    }
  }
  return output;
}

function sumArray(array) {
  return array.reduce((acc, el) => {
    return acc + el;
  }, 0);
}
function findBigrams(text, n) {
  const arrayOfWords = text.split(" ");
  const bigrams = [];
  for (let i = arrayOfWords.length; i >= n; i--) {
    bigrams.push(arrayOfWords.slice(i - n, i).join(" "));
  }
  return bigrams;
}

function containsNearbyDuplicate(array, topology) {
  const indexes = [];
  const checked = new Map();
  for (let i = 0, max = array.length; i < max; i++) {
    if (checked.has(array[i])) {
      if (Math.abs(i - checked.get(array[i]) <= topology)) {
        indexes.push([i, checked.get(array[i])]);
      }
      checked.set(array[i], i);
    }
  }
  return indexes;
}

function checkBrackets(expression) {
  let stack = [];
  for (let i = 0, max = expression.length; i < max; i++) {
    if (
      expression[i] === "(" ||
      expression[i] === "[" ||
      expression[i] == "{"
    ) {
      stack.push(expression[i]);
    }

    if (expression[i] == ")") {
      if (stack.pop() !== "(") {
        return false;
      }
    }

    if (expression[i] == "]") {
      if (stack.pop() !== "[") {
        return false;
      }
    }

    if (expression[i] == "}") {
      if (stack.pop() !== "{") {
        return false;
      }
    }
  }

  return stack.length === 0;
}

function analyzeText(text) {
  const cleanTest = text.replace(/[\.,-;]/g, "");
  const words = cleanTest.split(" ").map((word) => word.toLowerCase());
  if (text === "") words.pop();
  const sentences = text.split(/[\.!?]/);
  sentences.pop();
  const wordFrequency = {};
  for (let i = 0, max = words.length; i < max; i++) {
    if (wordFrequency[words[i]]) {
      wordFrequency[words[i]] += 1;
    } else {
      wordFrequency[words[i]] = 1;
    }
  }
  const totalWords = words.length;
  const uniqueWords = Object.keys(wordFrequency).length;
  const totalSentences = sentences.length;
  return {
    totalWords,
    uniqueWords,
    totalSentences,
    wordFrequency,
  };
}

function quickSort(arr) {
  if (arr.length === 0) return arr;
  let pivot = arr[0];
  const left = [];
  const right = [];
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] <= pivot) {
      left.push(arr[i]);
    } else {
      right.push(arr[i]);
    }
  }

  return [...quickSort(left), pivot, ...quickSort(right)];
}

function generateUniqueRandomNumbers(min, max, count) {
  const output = [];
}

function findMissingNumber2(arr) {
  if (arr[0] > 1) return arr[0] - 1;
  for (let i = 0, max = arr.length; i < max; i++) {
    if (arr[i + 1] !== arr[i] + 1) return arr[i] + 1;
  }

  return arr.length;
}

function capitalizeWords(sentence) {
  if (sentence === "") return "";
  return sentence
    .split(" ")
    .map((word) => word[0].toUpperCase() + word.slice(1))
    .join(" ");
}

function validateEmail(email) {
  const [pre, post] = email.split("@");
  if (!pre || !post) return false;
  if (post.indexOf(".") < 0) return false;
  const [postPre, postPost] = post.split(".");
  if (postPost === "") return false;
  if (!postPre.match(/[A-Za-a]/)) return false;
  if (!post.match(/[A-Za-z]/)) return false;
  return email.indexOf(" ") < 0;
}

function mergeArrays2(arr0, arr1) {
  let indicator0 = 0;
  let indicator1 = 0;
  const output = [];
  while (indicator0 < arr0.length && indicator1 < arr1.length) {
    if (arr0[indicator0] < arr1[indicator1]) {
      output.push(arr0[indicator0]);
      indicator0++;
      continue;
    }
    if (arr0[indicator0] > arr1[indicator1]) {
      output.push(arr1[indicator1]);
      indicator1++;
      continue;
    }
    if (arr0[indicator0] === arr1[indicator1]) {
      output.push(arr0[indicator0]);
      output.push(arr1[indicator1]);
      indicator0++;
      indicator1++;
    }
  }
  return output.concat(arr0.slice(indicator0)).concat(arr1.slice(indicator1));
}

function binarySearch(arr, target) {
  function divide(upper, lower) {
    return lower + Math.round((upper - lower) / 2);
  }

  if (arr.length === 0) return -1;
  let index = divide(arr.length - 1, 0);
  const boudaries = { lower: 0, upper: arr.length - 1 };
  while (true) {
    if (arr[index - 1] === target) {
      return index - 1;
    }

    if (arr[index] === target) {
      return index;
    }

    if (arr[index - 1] > target) {
      boudaries.upper = index - 1;
    }
    if (arr[index] < target) {
      boudaries.lower = index;
    }
    if (boudaries.lower === boudaries.upper && arr[index] !== target) {
      return -1;
    }
    index = divide(boudaries.upper, boudaries.lower);
  }
}

function findMaxConsecutiveOnes(arr) {
  let maxValue = 0;
  let currentValue = 0;

  for (let i = 0, max = arr.length; i < max; i++) {
    if (arr[i] === 1) {
      currentValue++;
      maxValue = Math.max(maxValue, currentValue);
    } else {
      currentValue = 0;
    }
  }
  return maxValue;
}

function validatePassword(password) {
  if (password.length < 8) return false;
  if (!password.match(/[A-Z]/)) return false;
  if (!password.match(/[a-z]/)) return false;
  if (!password.match(/[0-9]/)) return false;
  if (!password.match(/[!@#$%^&*()]/)) return false;
  return true;
}

function findDuplicate(arr) {
  return arr.find((el, index) => arr.indexOf(el) < index);
}

function findPeakElement(arr) {
  if (arr[0] >= arr[1] || !arr[1]) return 0;
  for (let i = 1, max = arr.length; i < max; i++) {
    if (arr[i - 1] <= arr[i] && arr[i] >= arr[i + 1]) {
      return i;
    }
  }
  if (arr[arr.length - 1] >= arr[arr.length - 2]) {
    return arr.length - 1;
  }
}

function maximumSubarraySum(arr, k) {
  if (arr.length < k) return 0;
  const sortedArrray = arr.sort((a, b) => a - b);
  let sum = 0;
  for (let i = arr.length - k; i < arr.length; i++) {
    sum += sortedArrray[i];
  }

  return sum;
}

function sortByStringLength(arr) {
  return arr.sort((a, b) => a.length - b.length);
}

function countTriplets(arr, target) {
  const triplets = [];
  const checked = {};
  for (let i = 0, max = arr.length; i < max - 1; i++) {
    for (let j = i + 1, max0 = arr.length; j < max0; j++) {
      let sum = arr[i] + arr[j];
      if (checked[target - sum]) {
        triplets.push([arr[i], arr[j], target - sum]);
      }
      checked[arr[i]] = true;
      checked[arr[j]] = true;
    }
  }
  return triplets;
}

function findCommonElements(arr0, arr1) {
  let i = 0,
    j = 0;
  const outputArray = [];
  while (true) {
    if (i >= arr0.length || j >= arr1.length) {
      return outputArray;
    }
    if (arr0[i] < arr1[j]) {
      i++;
      continue;
    }
    if (arr0[i] > arr1[j]) {
      j++;
      continue;
    }
    if (arr0[i] === arr1[j]) {
      if (outputArray[outputArray.length - 1] !== arr0[i]) {
        outputArray.push(arr0[i]);
      }

      i++;
      j++;
      continue;
    }
  }
}

function findUniqueElements(arr) {
  const checked = {};

  for (let i = 0; i < arr.length; i++) {
    if (typeof checked[arr[i]] === "undefined") {
      checked[arr[i]] = true;
    } else if (checked[arr[i]] === true) {
      checked[arr[i]] = false;
    }
  }

  return Object.keys(checked)
    .filter((el) => checked[el])
    .map((el) => {
      if (el === "true") return true;
      if (el === "false") return false;
      if (parseInt(el).toString() === "NaN") return el;
      return parseInt(el);
    });
}

function mergeSort(arr) {
  function mergeArrays2(arr0, arr1) {
    let indicator0 = 0;
    let indicator1 = 0;
    const output = [];
    while (indicator0 < arr0.length && indicator1 < arr1.length) {
      if (arr0[indicator0] < arr1[indicator1]) {
        output.push(arr0[indicator0]);
        indicator0++;
        continue;
      }
      if (arr0[indicator0] > arr1[indicator1]) {
        output.push(arr1[indicator1]);
        indicator1++;
        continue;
      }
      output.push(arr0[indicator0]);
      output.push(arr1[indicator1]);
      indicator0++;
      indicator1++;
    }
    return output.concat(arr0.slice(indicator0)).concat(arr1.slice(indicator1));
  }
  if (arr.length === 1 || arr.length === 0) {
    return arr;
  }

  let center = Math.floor(arr.length / 2);
  const arrCopy = [...arr];
  const rightArray = mergeSort(arrCopy.splice(center));
  const leftArray = mergeSort(arrCopy);
  return mergeArrays2(leftArray, rightArray);
}

function maxDiff(arr) {
  if (arr.length === 0) return 0;
  let minNumber = arr[0];
  let maxNumber = arr[0];
  for (let i = 1, max = arr.length; i < max; i++) {
    minNumber = Math.min(minNumber, arr[i]);
    maxNumber = Math.max(maxNumber, arr[i]);
  }

  return maxNumber - minNumber;
}

function removeDuplicates(arr) {
  const founded = {};
  const result = [];
  for (let i = 0, max = arr.length; i < max; i++) {
    if (!founded[arr[i]]) {
      result.push(arr[i]);
    }
    founded[arr[i]] = true;
  }
  return result;
}

function isIsogram(text) {
  const arr = text.split("");
  return new Set(arr).size === arr.length;
}

function findMaxDifference(array) {
  if (array.length <= 0) return null;
  const sortedArray = [...array].sort((a, b) => a - b);
  return Math.max(sortedArray.at(-1) - sortedArray[0]);
}

function maxProductOfTwo(array) {
  if (array.length < 2) return null;
  const sortedArray = array.sort((a, b) => a - b);
  return Math.max(
    sortedArray.at(-1) * sortedArray.at(-2),
    sortedArray.at(-1) * sortedArray[0],
    sortedArray[0] * sortedArray[1]
  );
}

function hasZeroSumTriplets(array) {
  const sortedArray = array.sort((a, b) => a - b);
  for (let i = 0, max = sortedArray.length - 2; i < max; i++) {
    let left = i + 1;
    let right = array.length - 1;

    while (left < right) {
      let sum = sortedArray[left] + sortedArray[right] + sortedArray[i];
      if (sum === 0) return true;
      if (sum < 0) left++;
      if (sum > 0) right--;
    }
  }

  return false;
}

function isPerfectNumber(number) {
  if (number === 0) return false;
  let sum = 0;
  for (let i = 1; i < number; i++) {
    if (number % i === 0) {
      sum += i;
    }
  }
  return sum === number;
}

const animals = [
  { name: "Elephant", type: "Mammal" },
  { name: "Lion", type: "Mammal" },
  { name: "Parrot", type: "Bird" },
  { name: "Snake", type: "Reptile" },
  { name: "Goldfish", type: "Fish" },
  { name: "Eagle", type: "Bird" },
];
function countAnimalsByType(type) {
  const grouped = Object.groupBy(animals, ({ type }) => type);
  const output = {};
  for (let type in grouped) {
    output[type] = grouped[type].length;
  }
  return output;
}

function findLongestWordWithoutSpecialCharacters(text) {
  const matched = text.match(/[A-Za-z0-9]+/g);
  return matched.reduce((acc, word) => {
    if (word.length > acc.length) return word;
    return acc;
  }, "");
}

function findShortestPath(graph, start, end) {
  const visited = [start];
  let next = [start];
  // ['A']->['AB', 'AC']
  // ['AB']->['AB A', 'AB C']
  while (true) {
    let nextNext = [];
    let addedNext = false;
    for (let i = 0, max = next.length; i < max; i++) {
      let neighborhood = graph[next[i].at(-1)];
      for (let j = 0, max0 = neighborhood.length; j < max0; j++) {
        let successor = neighborhood[j];
        if (visited.indexOf(successor) >= 0) {
          continue;
        }
        if (successor === end) {
          return (next[i] + successor).split("");
        }
        visited.push(successor);
        addedNext = true;
        nextNext.push(next[i] + successor);
      }
    }
    if (!addedNext) return null;

    next = [...nextNext];
  }
}

function findCycleInGraph(graph) {
  let precycles = Object.keys(graph);
  let next;
  while (true) {
    let addedNext = false;
    let nextPrecycles = [];
    for (let i = 0; i < precycles.length; i++) {
      for (j = 0, max0 = graph[precycles[i].at(-1)].length; j < max0; j++) {
        next = graph[precycles[i].at(-1)][j];
        if (precycles[i][0] === next) {
          return (precycles[i] + next).split("");
        }
        nextPrecycles.push(precycles[i] + next);
        addedNext = true;
      }
    }
    if (!addedNext) {
      return null;
    }
    precycles = [...nextPrecycles];
  }
}

function sumPairExists(arr, target) {
  const checked = new Map();
  for (let i = 0, max = arr.length; i < max; i++) {
    if (checked.has(target - arr[i])) {
      return true;
    }
    if (!checked.has(arr[i])) {
      checked.set(arr[i], true);
    }
  }
  return false;
}

function maxDifference(array) {
  let max = 0,
    min = Infinity,
    premin = Infinity;

  for (let i = 0; i < array.length; i++) {
    if (array[i] <= min) {
      premin = array[i];
    }
    if (array[i] >= max) {
      max = array[i];
      min = premin;
    }
  }
  if (min === Infinity && max === 0) {
    return 0;
  }
  return max - min;
}

function sortByDigitSum(array) {
  return array.sort((a, b) => {
    let digits0 = Math.abs(a).toString().split("").map(Number);
    let digits1 = Math.abs(b).toString().split("").map(Number);
    let sum0 = 0;
    let sum1 = 0;
    for (let i = 0, max = digits0.length; i < max; i++) {
      sum0 += digits0[i];
    }
    for (let i = 0, max = digits1.length; i < max; i++) {
      sum1 += digits1[i];
    }
    return sum0 - sum1;
  });
}

function numberOfUniqueness(array) {
  const checked = new Map();
  let output = array.length;
  for (let i = 0; i < array.length; i++) {
    if (checked.has(array[i])) {
      output--;
    }
    checked.set(array[i], true);
  }
  return output;
}
function maxProfit(prices) {
  if (prices.length < 2) {
    return 0;
  }

  let maxProfit = -Infinity;
  let minValue = prices[0];

  for (let i = 1; i < prices.length; i++) {
    minValue = Math.min(minValue, prices[i]);
    maxProfit = Math.max(maxProfit, prices[i] - minValue);
  }

  return maxProfit;
}

function findPairsWithDifference(array, target) {
  let elements = new Set(array);
  const checked = new Set();
  const pairs = new Set();
  for (let element of elements) {
    let differance0 = element - target;
    let differance1 = target + element;
    // target = element - differance
    // lub
    // target = differance - element

    // differance = element - target
    // lub
    // differance = target + element
    1;
    if (checked.has(differance0)) {
      pairs.add([element, differance0]);
    }
    if (checked.has(differance1)) {
      pairs.add([differance1, element]);
    }
    checked.add(element);
  }

  return Array.from(pairs);
}

function calculateSumExceptSelf(array) {
  const sum = array.reduce((sum, element) => sum + element, 0);
  const result = [];
  for (let i = 0, max = array.length; i < max; i++) {
    result.push(sum - array[i]);
  }

  return result;
}

function findEqualPairs(array) {
  const founded = {};
  const result = [];
  for (let i = 0, max = array.length; i < max; i++) {
    if (founded[array[i]]) {
      founded[array[i]].forEach((el) => {
        result.push([el, i]);
      });
      founded[array[i]].push(i);
    } else {
      founded[array[i]] = [i];
    }
  }

  return result;
}

function parsingDom(str) {
  const stack = [];
  while (str.match(/<\/[a-z]*>/)) {
    if (str.match(/^[A-Za-z\s]/)) {
      str = str.substring(1);
      continue;
    }
    let matched = str.match(/^<([a-z]+)>/);
    if (matched) {
      str = str.replace(matched[0], "");
      stack.push(matched[1]);
    }
    let matched0 = str.match(/^<\/([a-z]+)>/);
    if (matched0) {
      let opening = stack.pop();
      str = str.replace(/^<\/[a-z]+>/, "");
      if (opening === matched0[1]) {
        continue;
      } else {
        return opening;
      }
    }
  }
  return true;
}

function topologicalSort(graph) {
  const outputList = [];

  let degrees = {};

  function countDegress(graph) {
    const degrees = {};
    for (let i = 0, max = Object.keys(graph).length; i < max; i++) {
      degrees[Object.keys(graph)[i]] = 0;
    }
    for (let node in graph) {
      for (let i = 0, max = graph[node].length; i < max; i++) {
        degrees[graph[node][i]] += 1;
      }
    }

    return degrees;
  }

  function removeNode(graph, node) {
    delete graph[node];
    for (let el in graph) {
      let founded = graph[el].indexOf(node);
      if (founded >= 0) {
        graph[el].splice(founded, 1);
      }
    }
  }

  while (Object.keys(graph).length > 0) {
    degrees = countDegress(graph);
    for (let node in degrees) {
      if (degrees[node] === 0) {
        outputList.push(node);
        removeNode(graph, node);
        break;
      }
    }
  }

  return outputList;
}

function longestSubstringWithoutRepeating(str) {
  let maximum = 0;
  let array = str.split("");
  let start = 0,
    end = 0;

  while (end < array.length) {
    let window = array.slice(start, end + 1);
    if (window.length === new Set(window).size) {
      maximum = Math.max(maximum, end - start + 1);
      end += 1;
    } else {
      start += 1;
      end += 1;
    }
  }
  return maximum;
}

function decimalToBinary(num) {
  if (num === 0) return "0";
  let binary = "";
  while (num > 0) {
    binary = (num % 2) + binary;
    num = Math.floor(num / 2);
  }
  return binary;
}

function maxSubarraySumLessThanTarget(arr, target) {
  let start = 0,
    end = 0;
  let longest = [];
  let maxLength = 0;
  let currentValue = 0;
  while (end < arr.length) {
    currentValue += arr[end];
    while (currentValue > target && start <= end) {
      currentValue -= arr[start];
      start += 1;
    }
    if (end - start + 1 > maxLength && currentValue <= target) {
      maxLength = end - start + 1;
      longest = arr.slice(start, end + 1);
    }
    end++;
  }
  return longest;
}

function longestCommonSubsequenceLength(str0, str1) {
  const arr0 = str0.split("");
  const arr1 = str1.split("");
  const L = Array(arr0.length + 1)
    .fill(0)
    .map(() => Array(arr1.length + 1).fill(0));

  for (let i = 0, max = arr0.length; i < max; i++) {
    for (let j = 0, max0 = arr1.length; j < max0; j++) {
      if (arr0[i] !== arr1[j]) {
        L[i + 1][j + 1] = Math.max(L[i + 1][j], L[i][j + 1]);
      } else {
        L[i + 1][j + 1] = L[i][j] + 1;
      }
    }
  }
  return L[arr0.length][arr1.length];
}

function longestCommonSubsequence(str0, str1) {
  const arr0 = str0.split("");
  const arr1 = str1.split("");
  const L = Array(arr0.length + 1)
    .fill(0)
    .map(() => Array(arr1.length + 1).fill(0));

  for (let i = 0, max = arr0.length; i < max; i++) {
    for (let j = 0, max0 = arr1.length; j < max0; j++) {
      if (arr0[i] !== arr1[j]) {
        L[i + 1][j + 1] = Math.max(L[i + 1][j], L[i][j + 1]);
      } else {
        L[i + 1][j + 1] = L[i][j] + 1;
      }
    }
  }

  const outputArray = [];
  let i = arr0.length - 1;
  let j = arr1.length - 1;

  while (i >= 0 && j >= 0) {
    if (arr0[i] !== arr1[j]) {
      console.log(i, j);
      if (L[i + 1][j] > L[i][j + 1]) {
        j--;
      } else {
        i--;
      }
    } else {
      outputArray.unshift(arr0[i]);
      j--;
      i--;
    }
  }

  return outputArray.join("");
}

function findLongestIncreasingSubsequenceLength(array) {
  if (array.length === 0) return 0;
  const d = Array(array.length).fill(1);
  for (let i = 0, max = array.length; i < max; i++) {
    for (let j = 0; j < i; j++) {
      if (array[j] < array[i]) {
        d[i] = Math.max(d[i], d[j] + 1);
      }
    }
  }
  return Math.max(...d);
}

function findLongestIncreasingSubsequence2(array) {
  if (array.length === 0) return [];
  const d = Array(array.length).fill(1);
  const p = Array(array.length).fill(-1);
  for (let i = 0, max = array.length; i < max; i++) {
    for (let j = 0; j < i; j++) {
      if (array[j] < array[i] && d[i] < d[j] + 1) {
        d[i] = d[j] + 1;
        p[i] = j;
      }
    }
  }
  console.log(p);
  let pos = -1,
    maximum = -Infinity;
  for (let i = 0; i < d.length; i++) {
    if (d[i] > maximum) {
      pos = i;
      maximum = d[i];
    }
  }
  let i = pos;
  const reversed = [];
  while (i !== -1) {
    reversed.push(array[i]);
    i = p[i];
  }
  return reversed.reverse();
}

function gcd(a, b) {
  let c;
  while (b !== 0) {
    c = a % b;
    a = b;
    b = c;
  }
  return a;
}

function sumOfDigits(num) {
  return num
    .toString()
    .split("")
    .filter((el) => el !== "-")
    .map((el) => Number(el))
    .reduce((acc, el) => {
      return acc + el;
    }, 0);
}

function perfectNumbersLowerThan(num) {
  const perfectNumbers = [];
  for (let i = 0; i < num; i++) {
    if (isPerfectNumber(i)) {
      perfectNumbers.push(i);
    }
  }
  return perfectNumbers;
}

function longestPalindrome(s) {
  if (s.length < 2) {
    return s;
  }
  let maximal = "";
  let current0;
  let current1;
  let current;
  for (let i = 0, max = s.length; i < max; i++) {
    current0 = findLongestsAt(s, i, i);
    current1 = findLongestsAt(s, i, i + 1);
    current = current0.length > current1.length ? current0 : current1;
    if (current.length > maximal.length) {
      maximal = current;
    }
  }

  return maximal;

  function findLongestsAt(s, left, right) {
    while (
      left >= 0 &&
      right < s.length &&
      s.charAt(left) === s.charAt(right)
    ) {
      left--;
      right++;
    }
    left++;
    return s.slice(left, right);
  }
}

function longestVowelSubstring(string) {
  let maximum = 0;
  let current = 0;
  for (let i = 0, max = string.length; i < max; i++) {
    if (string[i].match(/[aeiou]/)) {
      current += 1;
      if (current > maximum) maximum = current;
    } else {
      current = 0;
    }
  }
  return maximum;
}

function countUniqueElements(array) {
  const founded = new Map();
  const unique = [];
  for (let i = 0; i < array.length; i++) {
    if (founded.has(array[i])) {
      founded.set(array[i], founded.get(array[i]) + 1);
    } else {
      founded.set(array[i], 1);
    }
  }
  for (let [key, value] of founded) {
    if (value === 1) {
      unique.push(key);
    }
  }

  return unique;
}

function countingSort(array) {
  let occured = new Array(101).fill(0);
  let output = [];
  for (let i = 0; i < array.length; i++) {
    occured[array[i]] += 1;
  }
  for (let i = 0; i < occured.length; i++) {
    for (let j = 0; j < occured[i]; j++) {
      output.push(i);
    }
  }

  return output;
}

function maxSubarraySum(arr) {
  if (arr.length === 0) return 0;
  let maxSoFar = arr[0];
  let maxEndingHere = arr[0];
  for (let i = 1, max = arr.length; i < max; i++) {
    maxEndingHere = Math.max(arr[i], maxEndingHere + arr[i]);
    maxSoFar = Math.max(maxSoFar, maxEndingHere);
  }

  return maxSoFar;
}

function countWords(str) {
  const array = str
    .split(" ")
    .filter((el) => el !== "")
    .map((el) => el.toLowerCase());
  const output = {};
  for (let i = 0, max = array.length; i < max; i++) {
    if (output[array[i]]) {
      output[array[i]] += 1;
    } else {
      output[array[i]] = 1;
    }
  }
  return output;
}

function binaryToDecimal(num) {
  const numbers = num.toString().split("").reverse();
  let decimal = 0;
  for (let i = 0; i < numbers.length; i++) {
    decimal += numbers[i] * 2 ** i;
  }
  return decimal;
}

function longestZeroSumSubarray(array) {
  const sums = new Map();
  let maxLength = 0;
  let startIdx = -1;
  let sum = 0;

  for (let i = 0, max = array.length; i < max; i++) {
    sum += array[i];
    if (sum === 0) {
      maxLength = i + 1;
      startIdx = 0;
    } else if (sums.has(sum)) {
      if (i - sums.get(sum) > maxLength) {
        maxLength = i - sums.get(sum);
        startIdx = sums.get(sum) + 1;
      }
    } else {
      sums.set(sum, i);
    }
  }
  return startIdx === -1 ? [] : array.slice(startIdx, startIdx + maxLength);
}

function longestSubstringWithoutRepeatingCharacters(str) {
  let output = "";
  const founded = new Map();

  for (let i = 0; i < str.length; i++) {
    if (!founded.has(str[i])) {
      output += str[i];
    }
    founded.set(str[i], true);
  }
  return output.length;
}

function treeConstructor(strArr) {
  const roots = new Map();
  const parentChild = new Map();
  const childParent = new Map();
  for (let i = 0, max = strArr.length; i < max; i++) {
    let [child, parent] = strArr[i].match(/\d+/g).map(Number);

    if (parentChild.has(parent)) {
      parentChild.get(parent).push(child);
    } else {
      parentChild.set(parent, [child]);
    }

    if (parentChild.get(parent).length > 2) {
      return false;
    }

    if (childParent.has(child)) {
      return false;
    }
    childParent.set(child, parent);

    if (roots.get(parent) !== false) {
      roots.set(parent, true);
    }
    roots.set(child, false);
  }
  // code goes here
  let find = false;
  for (const key of roots.keys()) {
    if (roots.get(key) && find) {
      return false;
    }
    if (roots.get(key)) {
      find = true;
    }
    //roots.set(key, true);
  }
  return find;
}

function StringChallenge(strArr) {
  let left = 0;
  let right = 0;

  const [source, pattern] = strArr;
  let answer = source + "a";

  function stringToMap(str) {
    const out = new Map();
    for (let i = 0, max = str.length; i < max; i++) {
      if (!out.has(str[i])) {
        out.set(str[i], 0);
      }
      out.set(str[i], out.get(str[i]) + 1);
    }
    return out;
  }
  const patternMap = stringToMap(pattern);

  function isValid(str) {
    const strMap = stringToMap(str);
    for (const key of patternMap.keys()) {
      if (
        typeof strMap.get(key) === "undefined" ||
        strMap.get(key) < patternMap.get(key)
      ) {
        return false;
      }
    }

    return true;
  }
  while (right < source.length) {
    let window = source.substring(left, right + 1);
    if (isValid(window)) {
      if (window.length < answer.length) {
        answer = window;
      }
      left++;
    } else {
      right++;
    }
  }
  // code goes here
  return answer;
}

function ArrayChallenge(arr) {
  arr.sort((a, b) => a - b);
  const maximum = arr.pop();
  let left = 0;
  let right = 0;
  let windowSum = arr[0];

  while (left < arr.length) {
    if (right === arr.length) return false;
    if (windowSum === maximum) {
      return true;
    }
    if (windowSum < maximum) {
      right++;
      windowSum = windowSum + arr[right];
    } else if (windowSum > maximum) {
      windowSum = windowSum - arr[left];
      left++;
    }
  }

  return false;
}

function lengthOfLongestSubstring(str) {
  const elements = new Set();
  let left = 0;
  let maxLength = 0;

  for(let i=0, max=str.length; i<max; i++) {
    while(elements.has(str[i])) {
      elements.delete(str[left]);
      left++;
    }
    elements.add(str[i]);
    maxLength = Math.max(maxLength, i-left+1);
  }
  
  return maxLength;
}

lengthOfLongestSubstring("pwwkew")

function maxSubarray(nums) {
  if (nums.length === 0) return [];

  let currentSum = nums[0];
  let maxSum=nums[0];
  let start=0;
  let end=0;
  let tempStart=0;

  for(let i=1; i<nums.length; i++) {
    if(currentSum+nums[i]>nums[i]) {
      currentSum+=nums[i];
    } else {
      currentSum=nums[i];
      tempStart=i;
    }
    if(currentSum>maxSum) {
      maxSum=currentSum;
      start = tempStart;
      end=i;
    }
  }
  

  return nums.slice(start, end + 1);
}

function missingNumber(arr) {
  const sum = arr.reduce((acc, current) => {
    return current + acc;
  }, 0);
  return (arr.length * (arr.length + 1)) / 2 - sum;
}

function longestIncreasingSubsequence(arr) {
  
  if (arr.length === 0) {
    return 0;
  }
  const dp=new Array(arr.length);
  dp.fill(1);

  for(let i=1, max=arr.length; i<max; i++){
    for(let j=0; j<i;j++) {

      if(arr[j]<arr[i]) {
        dp[i]=Math.max(dp[j]+1, dp[i]);
      }
    }
  }

  return Math.max(...dp);
}


function sumOfSquares(number) {
  const str = number.toString();
  let sum=0;
  for(let i=0, max=str.length; i<max; i++) {
    sum+=parseInt(str[i]) *parseInt(str[i]);
  }
  return sum;
}

function isHappyNumber(number, considered=new Set()) {
  
  if(number===1) {
    return true;
  } else {
    if(considered.has(number)) {
      return false;
    } else {
      considered.add(number);
      return isHappyNumber(sumOfSquares(number),considered);
    }
  }
}

function longestPalindromicSubstring(str) {
  const dp=new Array(str.length).fill(0).map(()=>new Array(str.length));
  
  let start
  let maxLength=1;
  for(let i=0,max=str.length;i<max;i++) {
    dp[i][i]=true;
    maxLength=1;
    start=i;
  }

  for(let i=0, max=str.length-1;i<max;i++) {
    if(str[i]===str[i+1]) {
      dp[i][i+1]=true;
      maxLength=2;
      start=i;
    }
  }
for(let length=3; length<=str.length; length++) {
  for(let i=0; i+length-1<=str.length; i++) {
    let j=i+length-1;
    if(dp[i+1][j-1] && str[i]===str[j]) {
      dp[i][j]=true;
      start=i;
      maxLength=length;
    }
  }
}

  return str.substr(start, maxLength);
}


function firstUniqChar(s) {
  let occurred = {};
  for(let i=0, max=s.length; i<max; i++) {
    if(typeof occurred[s[i]]!=='undefined') {
      occurred[s[i]]++;
    } else {
      occurred[s[i]] = 1;
    }
  }

  for(let i=0, max=s.length; i<max; i++) {
    if(occurred[s[i]]===1) {
      return i;
    }
  }
  return -1;
}

function findShortestPathMaze(maze) {
  function getDirections(i,j) {
    const directions=[];
    if(typeof maze[i+1]!=='undefined' && maze[i+1][j]===0) {
      directions.push([1,0]);
    }
    if(typeof maze[i][j+1]!=='undefined' && maze[i][j+1]===0) {
      directions.push([0,1]);
    }
    if(typeof maze[i-1]!=='undefined' && maze[i-1][j]===0) {
      directions.push([-1,0]);
    }
    if(maze[i][j-1]===0) {
      directions.push([0,-1]);
    }
    return directions;
  }

  function isValid(i,j) {
    return 0<=i && i<maze.length && 0<=j && j<maze[0].length;
  }

  function arraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length) return false;
    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) return false;
    }
    return true;
  }

  function containsArray(arrays, targetArray) {
    for (let array of arrays) {
      if (arraysEqual(array, targetArray)) {
        return true;
      }
    }
    return false;
  }

  maze[0][0] = true;
  let next=[[[0,0]]]
  const visited=[[0,0]]
  // [[[0,0]]]-> [[[0,0],[0,1]],[[0,0],[1,0]]]
  while(true) {
    let nextNext=[];
    let addedNext = false;
    
    for(let i=0, max=next.length; i<max; i++) { 
      letNextNext =[];
      let directions = getDirections(next[i].at(-1)[0], next[i].at(-1)[1]);
      let neighborhood=[];
      for(let k=0; k<directions.length;k++){
        if(isValid(next[i].at(-1)[0]+directions[k][0],next[i].at(-1)[1]+directions[k][1]) ) {
              neighborhood.push([next[i].at(-1)[0]+directions[k][0],next[i].at(-1)[1]+directions[k][1]])
          }
        }
      
        for(let j=0, max=neighborhood.length; j<max; j++) {
          if(containsArray(visited, neighborhood[j])) {
            continue;
          }
         if(neighborhood[j][0]===maze.length-1 && neighborhood[j][1]===maze[0].length-1) {
          return next[i].concat([neighborhood[j]]);
         }


         visited.push(neighborhood[j]);
         addedNext = true;
          nextNext.push(next[i].concat([neighborhood[j]]));
        }

        if(!addedNext) {
          return null;
        }
      }
      next = [...nextNext]
    }
  
}


module.exports = {
  findDuplicates,
  flattenArray,
  isPalindrome,
  duplicateCount,
  reverseWords,
  mergeArrays,
  findMissingNumber,
  rotateArray,
  findMinMax,
  firstNonRepeatingCharacter,
  sotrWord,
  findAllAnagrams,
  fibonacci,
  noRepetition,
  longestSubstring,
  areAnagrams,
  findIntersection,
  findMostFrequent,
  isPrimeNumber,
  reverseWordsInSentence,
  findMaxMinDifference,
  findPairsWithSum,
  isPermutationPalindrome,
  isPangram,
  findLongestNonDecraisingSubsequence,
  findMaxProductOfTwo,
  findLongestIncreasingSubsequence,
  findPairsWithSum2,
  findBigrams,
  checkBrackets,
  analyzeText,
  quickSort,
  findMissingNumber2,
  capitalizeWords,
  validateEmail,
  mergeArrays2,
  binarySearch,
  findMaxConsecutiveOnes,
  validatePassword,
  findDuplicate,
  findPeakElement,
  maximumSubarraySum,
  sortByStringLength,
  countTriplets,
  findCommonElements,
  findUniqueElements,
  mergeSort,
  maxDiff,
  removeDuplicates,
  isIsogram,
  findMaxDifference,
  maxProductOfTwo,
  hasZeroSumTriplets,
  isPerfectNumber,
  findShortestPath,
  findCycleInGraph,
  sumPairExists,
  maxDifference,
  sortByDigitSum,
  numberOfUniqueness,
  maxProfit,
  findPairsWithDifference,
  calculateSumExceptSelf,
  findEqualPairs,
  parsingDom,
  topologicalSort,
  longestSubstringWithoutRepeating,
  decimalToBinary,
  maxSubarraySumLessThanTarget,
  longestCommonSubsequenceLength,
  longestCommonSubsequence,
  findLongestIncreasingSubsequenceLength,
  findLongestIncreasingSubsequence2,
  gcd,
  sumOfDigits,
  perfectNumbersLowerThan,
  longestPalindrome,
  longestVowelSubstring,
  countUniqueElements,
  countingSort,
  maxSubarraySum,
  countWords,
  binaryToDecimal,
  longestZeroSumSubarray,
  longestSubstringWithoutRepeatingCharacters,
  treeConstructor,
  lengthOfLongestSubstring,
  maxSubarray,
  missingNumber,
  longestIncreasingSubsequence,
  isHappyNumber,
  longestPalindromicSubstring,
  firstUniqChar,
  findShortestPathMaze
  
};
